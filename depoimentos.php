<?php
include 'config.php';
define('TITLE', '');
define('PAGE', 'home');
define('SECTION_TITLE', 'Depoimentos');
define('SECTION_SUBTITLE', 'Mais de 5.000 alunos já passaram por nossas salas de aulas - físicas e virtuais');
define('SECTION_IMAGE', 'depoimentos.jpg');

elements('site/header');

elements('site/top-menu');
elements('site/menu');
elements('blocks/default');
?>

<section data-component="depoimentos">
    <div class="container">
        <div class="row">
            <?php for ( $i=0; $i<10; $i++ ) { ?>
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="depoimento" data-mh="depoimento">
                        <p>
                            Prezado Professor Andresan, o senhor não me conhece pessoalmente, porém fui sua aluna EAD e fiquei em primeiro no concurso TJRS para perito psiquiatra. Quero agradecer ao senhor pelas aulas e
                        </p>
                        <div class="d-flex">
                            <figure>
                                <img src="<?php echo BASE_URL . 'assets/images/alcina-barros.png' ?>" alt="">
                            </figure>
                            <h5>Alcina Barros</h5>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="text-center" >
            <button class="btn pink" type="button" data-toggle="collapse" data-target="#collapseDepoimento" aria-expanded="false" aria-controls="collapseExample">
                DEIXE SEU DEPOIMENTO
            </button>
        </div>
        <div class="collapse" id="collapseDepoimento">
            <div>
                <form class="" action="javascript:void(0)" method="post" data-validate>
                    <h3>Deixe seu depoimento</h3>
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <input type="text"
                                        name="nome"
                                        placeholder="NOME *"
                                        class="form-control"
                                        data-rule-required="true"
                                        data-msg-required="Campo obrigatório"
                                        >
                            </div><!-- END [ .FORM-GROUP ] -->
                        </div><!-- END [ .COLS ] -->
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <input type="email"
                                        name="email"
                                        placeholder="EMAIL *"
                                        class="form-control"
                                        data-rule-required="true"
                                        data-msg-required="Campo obrigatório"
                                        data-rule-email="true"
                                        data-msg-email="Preencha com um email válido"
                                        >
                            </div><!-- END [ .FORM-GROUP ] -->
                        </div><!-- END [ .COLS ] -->
                    </div><!-- END [ .ROW ] -->
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <textarea name="name" placeholder="DEPOIMENTO *" class="form-control" required="required" data-rule-required="true" data-msg-required="Campo obrigatório"></textarea>
                            </div>
                        </div><!-- END [ .COL ] -->
                    </div><!-- END [ .ROW ] -->
                    <button type="submit" class="btn pink">ENVIAR</button>
                </form>
            </div>
        </div>
    </div>
</section>


<?php
elements('site/social');

elements('site/form-inline');

elements('site/rodape');
elements('site/scripts');
elements('site/footer');
?>
