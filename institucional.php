<?php
include 'config.php';
define('TITLE', 'Institucional');
define('PAGE', 'institucional');
define('SECTION_TITLE', 'Institucional');
define('SECTION_SUBTITLE', '');
define('SECTION_IMAGE', 'cadastro.jpg');

elements('site/header');

elements('site/top-menu');
elements('site/menu');
elements('blocks/default');
?>

<section data-component="institucional">
    <div class="bg-white">
        <div class="intro container py-5">
            <h2 class="h2 text-red">ANDRESAN <small>CURSOS E CONCURSOS</small></h2>
            <hr class="xl thin pink">
            <p>A partir da necessidade de trazer ao mercado uma nova visão, mais moderna, inovadora e eficiente de aprendizagem, o Professor Andresan – Cursos para Concursos - consolidou sua marca no âmbito nacional ao longo de 20 anos como professor. Sendo referência em Português, foi além. Tratou de inovar, mais uma vez, e hoje conta com os melhores professores do mercado, para você conquistar o tão almejado cargo público e assegurar a sua estabilidade e de sua família.</p>
        </div>
    </div>

    <div class="container py-5">
        <h2 class="h3 text-center">NOSSOS PROFESSORES</h2>
        <hr class="xs red">

        <div class="teachers">
            <div class="teacher" data-toggler=".modal-teacher|active">
                <figure style="background-image: url(<?php echo BASE_URL . 'assets/images/professores/andresan.jpg' ?>);"></figure>
                <div class="caption" data-mh="caption-name">
                    <h3 class="name" data-name="Andresan">
                        Andresan Machado
                        <small>Português</small>
                    </h3>
                    <p class="d-none">
                        Pensou em Português para concursos?  Pensou no professor Andresan Machado. Há mais de 15 anos atuando no mercado, o professor é responsável por elevar o ensino da nossa língua pátria, trazendo uma visão mais moderna, inovadora e eficiente de aprendizagem. De forma diferenciada e com dicas especializadas para cada banca que você vai enfrentar, Andresan é um dos melhores no que faz.
                    </p>
                </div>
            </div>
            <div class="teacher" data-toggler=".modal-teacher|active">
                <figure style="background-image: url(<?php echo BASE_URL . 'assets/images/professores/angie.jpg' ?>);"></figure>
                <div class="caption" data-mh="caption-name">
                    <h3 class="name" data-name="Angie">
                        Angie Finkler
                        <small>Conhecimentos Específicos</small>
                    </h3>
                    <p class="d-none">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
            <div class="teacher" data-toggler=".modal-teacher|active">
                <figure style="background-image: url(<?php echo BASE_URL . 'assets/images/professores/kenny.jpg' ?>);"></figure>
                <div class="caption" data-mh="caption-name">
                    <h3 class="name" data-name="Kenny">
                        Kenny Martinez
                        <small>Legislação Tributária Estadual</small>
                    </h3>
                    <p class="d-none">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </p>
                </div>
            </div>
            <div class="teacher" data-toggler=".modal-teacher|active">
                <figure style="background-image: url(<?php echo BASE_URL . 'assets/images/professores/christian.jpg' ?>);"></figure>
                <div class="caption" data-mh="caption-name">
                    <h3 class="name" data-name="Christian">
                        Christian Azevedo
                        <small>Direito Administrativo</small>
                    </h3>
                    <p class="d-none">
                        Fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal-teacher">
    <div class="backdrop"></div>
    <main>
        <div class="container">
            <figure></figure>
            <div class="content">
                <h4>
                    Professor
                    <span></span>
                </h4>
                <p></p>
                <div class="text-center">
                    <a href="javascript:void(0)" class="btn white" data-toggler=".modal-teacher|active">
                        X FECHAR
                    </a>
                </div>
            </div>
        </div>
    </main>
</div>

<?php
elements('site/social');

elements('site/form-inline');

elements('site/rodape');
elements('site/scripts');
elements('site/footer');
?>
