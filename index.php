<?php
include 'config.php';
define('TITLE', '');
define('PAGE', 'home');
define('SECTION_TITLE', 'Explore novos concursos');
define('SECTION_SUBTITLE', 'Conheça nossos outros cursos');
define('COLUMNS', 4);
define('FILTER', false);

elements('site/header');

elements('site/top-menu');
elements('site/menu');
//
// elements('components/ao-vivo');
elements('blocks/block-small');
elements('components/cursos-destaque');
elements('components/cursos-destaque-2');
?>
<section data-layout="default">
<?php
elements('site/block_title');
elements('components/cloud-filter');
elements('components/select-filter');
elements('curso/listagem');
?>
</section>

<section data-component="block-default" style="background-image: url(<?php echo BASE_URL . 'assets/images/pexels-photo-135018.jpeg' ?>);">
    <div class="overlay pink">
        <div class="container text-center">
            <div class="block-title">
                Mais de 30.000 alunos
            </div>
            <div class="block-text">
                Já passaram por nossas salas de aulas - físicas e virtuais
            </div>
            <a href="<?php echo BASE_URL . 'depoimentos.php' ?>" class="btn outline white btn-sm">confira todos os depoimentos</a>
        </div>
    </div>
</section>

<?php
elements('components/conheca-o-andresan');
elements('site/blog');
elements('site/social');

elements('site/form-inline');

elements('site/rodape');
elements('site/scripts');
elements('site/footer');
?>
