<section data-component="curso-banner" style="background-image: url(<?php echo BASE_URL . 'assets/images/banner-curso.jpg' ?>);">
    <div class="overlay black">
        <div class="container">

            <h1 class="curso-title">
                <small>Concurso MPU</small>
                Analista Judiciário
            </h1>
            <div class="curso-diferencial">
                diferencial
            </div>

            <form data-component="form-curso" action="javascript:void(0)" method="post">
                <div class="form-group">
                    <label>Selecione a modalidade:</label>
                    <div class="fd-select-2" id="fds-modalidade" data-input="modalidade">
                        <div class="fds-header" data-toggler="#fds-modalidade|active">
                            <input type="text" name="modalidade" value="Presencial">
                            <span>Presencial</span>
                        </div>
                        <div class="fds-body">
                            <ul>
                                <li data-value="presencial" class="selected">Presencial</li>
                                <li data-value="semipresencial">Semipresencial</li>
                                <li data-value="ead">EAD</li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end [ .FORM-GROUP ] -->

                <div class="form-group">
                    <label>Selecione o turno desejado:</label>
                    <div class="fd-select-2" id="fds-turno" data-input="turno">
                        <div class="fds-header" data-toggler="#fds-turno|active">
                            <input type="text" name="turno" value="Manhã">
                            <span>Manhã</span>
                        </div>
                        <div class="fds-body">
                            <ul>
                                <li data-value="noite" class="selected">Manhã</li>
                                <li data-value="tarde">Tarde</li>
                                <li data-value="noite">Noite</li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end [ .FORM-GROUP ] -->

                <div class="form-group">
                    <label>Selecione o tipo do curso:</label>
                    <div class="fd-select-2" id="fds-tipo" data-input="tipo">
                        <div class="fds-header" data-toggler="#fds-tipo|active">
                            <input type="text" name="tipo" value="Intensivo">
                            <span>Intensivo</span>
                        </div>
                        <div class="fds-body">
                            <ul>
                                <li data-value="extensivo">Extensivo</li>
                                <li data-value="semiextensivo">Semiextensivo</li>
                                <li data-value="intensivo" class="selected">Intensivo</li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end [ .FORM-GROUP ] -->

                <div>
                    <p class="banner-price">
                        <span>R$</span> 1.090<strong>,00</strong>
                        <small class="d-block">ou 10x de R$109,00 sem juros</small>
                    </p>
                </div>

                <a href="<?php echo BASE_URL . 'checkout.php' ?>" class="btn green">comprar curso</a>
            </form>

            <figure class="banner-badge">
                <img src="<?php echo BASE_URL . 'assets/images/mpu.jpg' ?>" alt="MPU">
            </figure>
        </div><!-- end [ .CONTAINER ] -->
    </div><!-- end [ .OVERLAY.BLACK ] -->
</section>
