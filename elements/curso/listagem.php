<section data-component="courses">
    <div class="container">
        <div class="cards filter-courses" data-columns="<?php echo COLUMNS ?>" data-grid="grid">

            <a href="<?php echo BASE_URL . 'curso.php' ?>" class="card" data-mh="curso" data-category="categoria-b">
                <figure class="overlay">
                    <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                </figure>
                <div class="card-body px-3 px-md-5">
                    <div class="card-badge">
                        <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                    </div>
                    <h5 class="card-title">Analista Judiciário</h5>
                    <p class="card-text">Matricule-se! Em andamento.</p>

                    <div class="card-value">
                        <span>De R$2.000,00</span>
                        <strong>R$1.500,00</strong>
                        <small>ou 10x de R$150,00 sem juros</small>
                    </div>
                </div>
                <div class="card-footer px-3 px-md-5">
                    <div class="btn">veja mais detalhes</div>
                </div>
            </a>

            <a href="<?php echo BASE_URL . 'curso.php' ?>" class="card" data-mh="curso" data-category="categoria-a">
                <figure class="overlay">
                    <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                </figure>
                <div class="card-body px-3 px-md-5">
                    <div class="card-badge">
                        <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                    </div>
                    <h5 class="card-title">Analista Judiciário</h5>
                    <p class="card-text">Matricule-se! Em andamento.</p>

                    <div class="card-value">
                        <span>De R$2.000,00</span>
                        <strong>R$1.500,00</strong>
                        <small>ou 10x de R$150,00 sem juros</small>
                    </div>
                </div>
                <div class="card-footer px-3 px-md-5">
                    <div class="btn">veja mais detalhes</div>
                </div>
            </a>

            <a href="<?php echo BASE_URL . 'curso.php' ?>" class="card" data-mh="curso" data-category="categoria-c">
                <figure class="overlay">
                    <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                </figure>
                <div class="card-body px-3 px-md-5">
                    <div class="card-badge">
                        <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                    </div>
                    <h5 class="card-title">Analista Judiciário</h5>
                    <p class="card-text">Matricule-se! Em andamento.</p>

                    <div class="card-value">
                        <span>De R$2.000,00</span>
                        <strong>R$1.500,00</strong>
                        <small>ou 10x de R$150,00 sem juros</small>
                    </div>
                </div>
                <div class="card-footer px-3 px-md-5">
                    <div class="btn">veja mais detalhes</div>
                </div>
            </a>

            <a href="<?php echo BASE_URL . 'curso.php' ?>" class="card" data-mh="curso" data-category="categoria-a">
                <figure class="overlay">
                    <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                </figure>
                <div class="card-body px-3 px-md-5">
                    <div class="card-badge">
                        <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                    </div>
                    <h5 class="card-title">Analista Judiciário</h5>
                    <p class="card-text">Matricule-se! Em andamento.</p>

                    <div class="card-value">
                        <span>De R$2.000,00</span>
                        <strong>R$1.500,00</strong>
                        <small>ou 10x de R$150,00 sem juros</small>
                    </div>
                </div>
                <div class="card-footer px-3 px-md-5">
                    <div class="btn">veja mais detalhes</div>
                </div>
            </a>

        </div>

        <div class="text-center">
            <a href="<?php echo BASE_URL . 'cursos.php' ?>" class="link default">Veja todos os cursos disponíveis</a>
        </div>
    </div>
</section>
