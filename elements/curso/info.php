<section data-component="curso-info">
    <div class="container">
        <div class="items">
            <div class="item-logo bg-dark-pink">
                <img src="<?php echo BASE_URL . 'assets/images/andresan-branco.png' ?>" alt="Andresan Cursos e Concursos">
            </div>
            <div class="item-nome bg-dark-pink">
                <p>
                    <small class="d-block">Concurso MPU</small>
                    Analista Judiciário
                </p>
            </div>
            <div class="item-modalidade bg-dark-pink text-sm-center">
                <p>
                    <small class="d-block">Modalidade do curso</small>
                    Presencial
                </p>
            </div>
            <div class="item-turno bg-dark-pink text-sm-center">
                <p>
                    <small class="d-block">Turno</small>
                    Manhã
                </p>
            </div>
            <div class="item-tipo bg-dark-pink text-sm-center">
                <p>
                    <small class="d-block">Tipo</small>
                    Extensivo
                </p>
            </div>
            <div class="item-price bg-pink text-sm-center">
                <p>
                    <span><strong>R$</strong> 1.090<strong>,00</strong></span>
                    <small class="d-block">ou 10x de R$109,00 sem juros</small>
                </p>
            </div>
            <div class="item-cart bg-pink">
                <a href="<?php echo BASE_URL . 'checkout.php' ?>" class="btn green">
                    comprar curso
                </a>
            </div>
        </div>
    </div>
</section>
