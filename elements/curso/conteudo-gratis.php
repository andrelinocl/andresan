<main data-component="main-content">
    <div class="container">
        <section data-component="tab-accordion">

            <ul class="nav nav-tabs mb-5" id="curso-tabs" role="tablist">
                <li class="nav-item">
                    <a href="#pane-A" id="tab-A" class="nav-link active show" data-toggle="tab" role="tab">O Curso</a>
                </li>
                <li class="nav-item">
                    <a href="#pane-B" id="tab-B" class="nav-link" data-toggle="tab" role="tab">O concurso</a>
                </li>
                <li class="nav-item">
                    <a href="#pane-C" id="tab-C" class="nav-link" data-toggle="tab" role="tab">Professores</a>
                </li>
                <li class="nav-item">
                    <a href="#pane-D" id="tab-D" class="nav-link" data-toggle="tab" role="tab">Informações importantes</a>
                </li>
            </ul>

            <hr class="thin bg-gray-100">

            <div id="accordion-curso" class="tab-content" role="tablist">

                <div class="card tab-pane fade active show" id="pane-A" role="tabpanel" aria-labelledby="tab-A">
                    <button class="btn card-header" data-toggle="collapse" data-target="#content-A" aria-expanded="true" aria-controls="content-A" id="collapse-A">
                        O Curso
                    </button>

                    <div id="content-A" class="collapse show" aria-labelledby="collapse-A" data-parent="#accordion-curso">
                        <div class="card-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>

                <div class="card tab-pane fade" id="pane-B" role="tabpanel" aria-labelledby="tab-B">
                    <button class="btn card-header collapsed" data-toggle="collapse" data-target="#content-B" aria-expanded="true" aria-controls="content-B" id="collapse-B">
                        O Concurso
                    </button>

                    <div id="content-B" class="collapse" aria-labelledby="collapse-B" data-parent="#accordion-curso">
                        <div class="card-body">
                            <h3>Lorem Ipsum</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="card tab-pane fade" id="pane-C" role="tabpanel" aria-labelledby="tab-C">
                    <button class="btn card-header collapsed" data-toggle="collapse" data-target="#content-C" aria-expanded="true" aria-controls="content-C" id="collapse-C">
                        Professores
                    </button>

                    <div id="content-C" class="collapse" aria-labelledby="collapse-C" data-parent="#accordion-curso">
                        <div class="card-body">
                            <h3>LOREM IPSUM</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, nobis optio! A assumenda cumque debitis dolorum earum, error inventore ipsum molestias, obcaecati quam quasi quidem recusandae soluta temporibus ullam veritatis.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi architecto culpa, doloribus et inventore iure iusto nesciunt obcaecati officia optio qui quia quibusdam rerum sit sunt temporibus veniam voluptates.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="card tab-pane fade" id="pane-D" role="tabpanel" aria-labelledby="tab-D">
                    <button class="btn card-header collapsed" data-toggle="collapse" data-target="#content-D" aria-expanded="true" aria-controls="content-D" id="collapse-D">
                        Informações Importantes
                    </button>

                    <div id="content-D" class="collapse" aria-labelledby="collapse-D" data-parent="#accordion-curso">
                        <div class="card-body">

                            <h3>LOREM IPSUM</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, nobis optio! A assumenda cumque debitis dolorum earum, error inventore ipsum molestias, obcaecati quam quasi quidem recusandae soluta temporibus ullam veritatis.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi architecto culpa, doloribus et inventore iure iusto nesciunt obcaecati officia optio qui quia quibusdam rerum sit sunt temporibus veniam voluptates.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php elements('curso/sidebar-gratis'); ?>
    </div>
</main>
