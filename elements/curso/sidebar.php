<aside class="sidebar-curso">
    <p class="sidebar-price">
        <span><strong>R$</strong> 1.090<strong>,00</strong></span>
        <small class="d-block">ou 10x de R$109,00 sem juros</small>
    </p>
    <hr class="thin bg-gray-100">

    <p class="text-center"><small>outras formas de pagamento</small></p>

    <div class="sidebar-item mb-3">
        <figure>
            <img src="<?php echo BASE_URL . 'assets/images/icons/credit-card.svg' ?>" class="fd-svg">
        </figure>
        <p>
            parcelamento no cartão
            <small>(Até 10x sem juros)</small>
        </p>
    </div>

    <div class="sidebar-item mb-3">
        <figure>
            <img src="<?php echo BASE_URL . 'assets/images/icons/barcode.svg' ?>" class="fd-svg">
        </figure>
        <p>
            parcelamento no boleto
            <small>(Até 4x sem juros)</small>
        </p>
    </div>
    <div class="mt-3 mt-lg-5">
        <a href="<?php echo BASE_URL . 'checkout.php' ?>" class="btn green">comprar curso</a>
    </div>

</aside>
