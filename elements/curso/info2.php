<section data-component="curso-info-2">
    <div class="container">
        <div class="items">
            <div class="item">
                <figure>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/multimedia.svg' ?>" class="fd-svg">
                </figure>
                <p>
                    <small class="d-block">Número de videoaulas</small>
                    477 videoaulas
                </p>
            </div>
            <div class="item">
                <figure>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/small-calendar.svg' ?>" class="fd-svg">
                </figure>
                <p>
                    <small class="d-block">Acesso até</small>
                    21/10
                </p>
            </div>
            <div class="item">
                <figure>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/time.svg' ?>" class="fd-svg">
                </figure>
                <p>
                    <small class="d-block">Carga Horária</small>
                    424h/aula
                </p>
            </div>
            <div class="item">
                <figure>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/wallet.svg' ?>" class="fd-svg">
                </figure>
                <p>
                    <small class="d-block">Remuneração</small>
                    R$ 11.125,00
                </p>
            </div>
            <div class="item">
                <figure>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/hourglass.svg' ?>" class="fd-svg">
                </figure>
                <p>
                    <small class="d-block">Tempo de Aula</small>
                    20-30 min
                </p>
            </div>
            <div class="item">
                <figure>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/placeholder.svg' ?>" class="fd-svg">
                </figure>
                <p>
                    <small class="d-block">Local</small>
                    Praia de Belas
                </p>
            </div>
        </div>
    </div>
</section>
