<aside class="sidebar-curso">
    <form class="sidebar-form" action="javascript:void(0)" method="post" data-validate>
        <p class="text-pink">Preencha todos os campos e adquira este curso</p>
        <div class="form-group">
            <input type="text"
                    name="nome"
                    placeholder="NOME"
                    class="form-control"
                    required="required"
                    data-rule-required="true"
                    data-msg-required="Preenchimento obrigatório"
                    >
        </div>
        <div class="form-group">
            <input type="email"
                    name="email"
                    placeholder="EMAIL"
                    required="required"
                    class="form-control"
                    data-rule-required="true"
                    data-msg-required="Preenchimento obrigatório"
                    data-rule-email="true"
                    data-msg-email="Preencha com um email válido"
                    >
        </div>
        <div class="form-group">
            <input type="text"
                    name="telefone"
                    placeholder="TELEFONE"
                    class="form-control"
                    required="required"
                    data-rule-required="true"
                    data-msg-required="Preenchimento obrigatório"
                    data-rule-minlength="14"
                    data-msg-minlength="Preencha com um número válido"
                    data-mask="phone">
        </div>
        <button type="submit" class="btn pink">ADQUIRIR</button>
    </form>

</aside>
