<div class="container">
    <p class="d-md-none">
        <a class="btn btn-filter collapsed" data-toggle="collapse" href="#filterCollapse" role="button" aria-expanded="false" aria-controls="collapseExample">
            Selecione um ou mais tipos de curso
            <i class="fas fa-filter"></i>
        </a>
    </p>
    <div class="collapse" id="filterCollapse">
        <div>
            <div class="button-filter">
                <a href="javascript:void(0)" class="btn active">Tribunal de Justiça</a>
                <a href="javascript:void(0)" class="btn">Português Extensivo</a>
                <a href="javascript:void(0)" class="btn">SEFAZ</a>
                <a href="javascript:void(0)" class="btn active">Ministério Público da União</a>
                <a href="javascript:void(0)" class="btn">Exame da Ordem OAB</a>
                <a href="javascript:void(0)" class="btn">DEPEN</a>
                <a href="javascript:void(0)" class="btn">CESPE/CEBRASPE</a>
                <a href="javascript:void(0)" class="btn">Tribunal Regional Federal </a>
                <a href="javascript:void(0)" class="btn">Polícia Rodoviária Federal</a>
                <a href="javascript:void(0)" class="btn">FAURGS</a>
                <a href="javascript:void(0)" class="btn">Fundação Carlos Chagas (FCC)</a>
                <a href="javascript:void(0)" class="btn">FUNDATEC</a>
                <a href="javascript:void(0)" class="btn">INSS</a>
                <a href="javascript:void(0)" class="btn">LA SALLE</a>
            </div>
        </div>
    </div>

</div>
