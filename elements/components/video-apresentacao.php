<section data-component="block-default" class="video-apresentacao" style="background-image: url(<?php echo BASE_URL . 'assets/images/woman-typing-writing-windows.jpg' ?>);">
    <div class="overlay pink">
        <div class="container text-center">
            <figure>
                <img src="<?php echo BASE_URL . 'assets/images/icons/play-button.svg'; ?>" alt="" class="fd-svg">
            </figure>
            <p class="text-white mb-0">
                <small>veja agora o vídeo e conheça mais sobre este curso</small>
            </p>
            <h2 class="h3 text-white">video de apresentação</h2>
            <a href="#" class="btn outline white mt-4 mt-xl-5 px-5">assistir ao vídeo</a>
        </div>
    </div>
</section>
