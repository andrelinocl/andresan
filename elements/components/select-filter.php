<div class="container">
    <div class="filter">
        <div class="left">
            <div class="fd-select select__categoria">
                <div class="fd-select-title">
                    Categoria <span>Selecione</span> <i class="fas fa-angle-down"></i>
                </div>
                <ul>
                    <li><a href="javascript:void(0)" data-category="categoria-a">Categoria A</a></li>
                    <li><a href="javascript:void(0)" data-category="categoria-a">Categoria B</a></li>
                    <li><a href="javascript:void(0)" data-category="categoria-c">Categoria C</a></li>
                    <li><a href="javascript:void(0)" data-category="categoria-d">Categoria D</a></li>
                </ul>
            </div>

            <div class="fd-select select__modalidade">
                <div class="fd-select-title">
                    Modalidade <span>Curso EAD</span> <i class="fas fa-angle-down"></i>
                </div>
                <ul>
                    <li><a href="" data-modality="ead">Cursos EAD</a></li>
                    <li><a href="" data-modality="presencial">Cursos Presenciais</a></li>
                    <li><a href="" data-modality="portugues">Cursos de Português</a></li>
                    <li><a href="" data-modality="gratis">Cursos Grátis</a></li>
                </ul>
            </div>

            <div class="fd-select select__curso">
                <div class="fd-select-title">
                    Tipo de curso <span>Intensivo</span> <i class="fas fa-angle-down"></i>
                </div>
                <ul>
                    <li><a href="" data-course="extensivo">Extensivo</a></li>
                    <li><a href="" data-course="semiextensivo">Semiextensivo</a></li>
                    <li><a href="" data-course="intensivo">Intensivo</a></li>
                </ul>
            </div>
        </div>
        <div class="view-mode">
            <a href="#" title="Grid" class="selected" data-target="@courses .cards" data-style="grid">
                <i class="fas fa-th"></i>
                <span>Cards</span>
            </a>
            <a href="#" title="Lista" data-target="@courses .cards" data-style="list">
                <i class="fas fa-th-list"></i>
                <span>Lista</span>
            </a>
        </div>
    </div>
</div>
