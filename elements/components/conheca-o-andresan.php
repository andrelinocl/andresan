<section data-component="block-default" class="teacher-home">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 order-sm-2 center-content" data-mh="column">
                <div>
                    <h2 class="block-title">
                        <span class="d-block text-gray-900">Professor</span>
                        <span class="text-red">Andresan</span>
                    </h2>
                    <p class="block-text text-black text-left">
                        A partir da necessidade de trazer ao mercado uma nova visão, mais moderna, inovadora e eficiente de aprendizagem, o Professor Andresan – Cursos para Concursos - consolidou sua marca no âmbito nacional ao longo de 20 anos como professor. Sendo referência em Português, foi além. Tratou de inovar, mais uma vez, e hoje conta com os melhores professores do mercado, para você conquistar o tão almejado cargo público e assegurar a sua estabilidade e de sua família.
                    </p>
                    <div class="d-flex buttons">
                        <a href="<?php echo BASE_URL . 'institucional.php' ?>" class="btn outline gray-900">conheça mais</a>
                        <a href="<?php echo BASE_URL . 'institucional.php' ?>" class="btn pink">conheça todos os professores</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 order-sm-1 px-0" data-mh="column">
                <div class="d-flex justify-content-end">
                    <img src="<?php echo BASE_URL . 'assets/images/andresan-foto.png' ?>" alt="Andresan" class="mt-sm--60">
                </div>
            </div>
        </div>
    </div>
</section>
