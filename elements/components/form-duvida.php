<section data-component="block-default" style="background-image: url(<?php echo BASE_URL . 'assets/images/pexels-photo-135018.jpeg' ?>);">
    <div class="overlay black">
        <div class="container text-center">
            <div class="block-title">
                <small class="tiny">Tem alguma dúvida?</small>
            </div>
            <div class="block-text">
                Preencha os campos abaixo para receber mais informações sobre este curso
            </div>
            <form action="javascript:void(0)" method="POST" data-form="duvidas" data-validate>
                <div class="row">
                    <div class="col-12 col-md">
                        <div class="form-group">
                            <input type="text"
                                    name="nome"
                                    class="form-control"
                                    placeholder="Seu nome"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="preencha o campo corretamente"
                                    >
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md">
                        <div class="form-group">
                            <input type="email"
                                    name="email"
                                    class="form-control"
                                    placeholder="Seu E-mail"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="preencha o campo corretamente"
                                    data-rule-email="true"
                                    data-msg-email="Preencha o campo com um e-mail válido"
                                    >
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md">
                        <div class="form-group">
                            <input type="text"
                                    name="telefone"
                                    class="form-control"
                                    placeholder="Seu telefone"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="preencha o campo corretamente"
                                    data-rule-minlength="14"
                                    data-msg-minlength="Preencha o campo com um número válido"
                                    data-mask="phone"
                                    >
                        </div>
                    </div>
                </div><!-- end [ .ROW ] -->
                <div class="row">
                    <div class="col-12 col-sm-8">
                        <textarea name="name" class="form-control" placeholder="Sua mensagem" required="required" data-rule-required="true" data-msg-required="Preencha o campo corretamente"></textarea>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button type="submit" class="btn pink">enviar contato</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
