<section data-component="courses-home">
    <div class="container-fluid px-0">
        <div class="row no-gutters">
            <div class="col-12 col-sm-6">
                <div class="item item-1" style="background-image: url(<?php echo BASE_URL . 'assets/images/item-1.jpg' ?>);">
                    <a href="<?php echo BASE_URL . 'curso.php' ?>" class="item-content">
                        <span class="item-title">
                            Analista Judiciário
                            <strong><small>R$</small>790,00</strong>
                            <small>ou 10x de R$ 79,00 sem juros</small>
                        </span>
                        <div class="item-badge d-none" data-position="bottom-right">
                            <img src="<?php echo BASE_URL . '/assets/images/mpu.jpg' ?>" alt="">
                        </div>
                    </a>
                </div><!-- END [ .TIEM ] -->

                <div class="item item-2" style="background-image: url(<?php echo BASE_URL . 'assets/images/item-2.jpg' ?>);">
                    <a href="<?php echo BASE_URL . 'curso.php' ?>" class="item-content">
                        <span class="item-title">
                            Auditor Fiscal
                            <strong><small>R$</small>990,00</strong>
                            <small>ou 10x de R$ 99,00 sem juros</small>
                        </span>
                        <div class="item-badge d-none" data-position="bottom-right">
                            <img src="<?php echo BASE_URL . '/assets/images/mpu.jpg' ?>" alt="">
                        </div>
                    </a>
                </div><!-- END [ .ITEM ] -->
            </div><!-- END [ .COLS ] -->
            <div class="col-12 col-sm-6">
                <div class="item item-3" style="background-image: url(<?php echo BASE_URL . 'assets/images/item-3.jpg' ?>);">
                    <a href="<?php echo BASE_URL . 'curso.php' ?>" class="item-content">
                        <span class="item-title">
                            Técnico Judiciário
                            <strong><small>R$</small>590,00</strong>
                            <small>ou 10x de R$ 59,00 sem juros</small>
                        </span>
                        <div class="item-badge d-none" data-position="bottom-left">
                            <img src="<?php echo BASE_URL . '/assets/images/mpu.jpg' ?>" alt="">
                        </div>
                    </a>
                </div><!-- END [ .ITEM ] -->

                <div class="row no-gutters">
                    <div class="col-12 col-lg-6">
                        <div class="item item-4" style="background-image: url(<?php echo BASE_URL . 'assets/images/item-4.jpg' ?>);">
                            <a href="<?php echo BASE_URL . 'curso.php' ?>" class="item-content">
                                <span class="item-title">
                                    Técnico Administrativo
                                    <strong><small>R$</small>590,00</strong>
                                    <small>ou 10x de R$ 59,00 sem juros</small>
                                </span>
                                <div class="item-badge d-none" data-position="bottom-right">
                                    <img src="<?php echo BASE_URL . '/assets/images/mpu.jpg' ?>" alt="">
                                </div>
                            </a>
                        </div><!-- END [ .ITEM ] -->
                    </div><!-- END [ .COLS ] -->
                    <div class="col-12 col-lg-6">
                        <a href="<?php echo BASE_URL . 'cursos.php' ?>" class="item link">
                            Conheça todos<br />os nossos cursos
                        </a>
                    </div><!-- END [ .COLS ] -->
                </div>
            </div>
        </div>
    </div>
</section>
