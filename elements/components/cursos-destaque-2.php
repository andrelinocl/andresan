<section data-component="spotlight-home" data-layout="default">
    <div class="container">
        <h2 class="section-title text-white">Prepare-se para os concursos</h2>
        <p class="section-subtitle text-white">Confira os concursos em destaque</p>
        <hr class="xs red">

        <div class="owl-carousel" data-carousel="spotlight-home">
            <div class="slick-item">
                <div style="background-image: url(<?php echo BASE_URL . 'assets/images/pexels-photo-1020313.jpeg' ?>);">
                    <div class="spotlight-content">
                        <div class="spotlight-badge"><img src="<?php echo BASE_URL . 'assets/images/mpu.jpg' ?>" alt=""></div>
                        <div class="spotlight-type">CONCURSO</div>
                        <div class="spotlight-name">Exame de Ordem</div>
                        <a href="javascript:void(0)" data-toggler=".spotlight_modal|active" class="spotlight-cta">Veja os cursos disponíveis</a>
                    </div>
                </div>
            </div>

            <div class="slick-item">
                <div style="background-image: url(<?php echo BASE_URL . 'assets/images/pexels-photo-1020313.jpeg' ?>);">
                    <div class="spotlight-content">
                        <div class="spotlight-badge"><img src="<?php echo BASE_URL . 'assets/images/mpu.jpg' ?>" alt=""></div>
                        <div class="spotlight-type">CONCURSO</div>
                        <div class="spotlight-name">Tribunal de Justiça</div>
                        <a href="javascript:void(0)" data-toggler=".spotlight_modal|active" class="spotlight-cta">Veja os cursos disponíveis</a>
                    </div>
                </div>
            </div>

            <div class="slick-item">
                <div style="background-image: url(<?php echo BASE_URL . 'assets/images/pexels-photo-1020313.jpeg' ?>);">
                    <div class="spotlight-content">
                        <div class="spotlight-badge"><img src="<?php echo BASE_URL . 'assets/images/mpu.jpg' ?>" alt=""></div>
                        <div class="spotlight-type">CONCURSO</div>
                        <div class="spotlight-name">Ministério Público da União</div>
                        <a href="javascript:void(0)" data-toggler=".spotlight_modal|active" class="spotlight-cta">Veja os cursos disponíveis</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="spotlight_modal">
        <div class="spotlight_modal--header">
            <h2 class="section-title text-white">Prepare-se para os concursos</h2>
            <p class="section-subtitle text-white">Confira os concursos em destaque</p>
            <hr class="xs red">
        </div>

        <div class="container">
            <a href="javascript:void(0)" class="spotlight_modal--close" data-toggler=".spotlight_modal|active">
                <img src="<?php echo BASE_URL . 'assets/images/icons/close-button.svg' ?>" class="fd-svg">
            </a>
            <div class="owl-carousel" data-carousel="spotlight-modal">
                <?php for( $i=0; $i<4; $i++ ): ?>
                    <div class="item">
                        <div class="spotlight_modal--item">
                            <div class="spotlight_modal--figure" style="background-image: url(<?php echo BASE_URL . 'assets/images/pexels-photo-1020313.jpeg' ?>);">
                                <div class="spotlight_modal--badge"><img src="<?php echo BASE_URL . 'assets/images/mpu.jpg' ?>" alt=""></div>
                                <div class="spotlight_modal--type">CONCURSO</div>
                                <div class="spotlight_modal--name">Tribunal de Justiça</div>
                                <a href="<?php echo BASE_URL . 'curso.php' ?>" class="spotlight_modal--cta">Veja os cursos disponíveis</a>

                                <div class="spotlight_modal--buttons">
                                    <div class="spotlight_modal--prev">
                                        <i class="fas fa-chevron-left"></i>
                                    </div>
                                    <div class="spotlight_modal--next">
                                        <i class="fas fa-chevron-right"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="spotlight_modal--ctas">
                                <a href="#" class="card">
                                    <h5 class="card-title">Assistente Administrativo Fazendário</h5>
                                    <p class="card-text">Matricule-se! Em andamento.</p>

                                    <div class="card-value">
                                        <span>De R$2.000,00</span>
                                        <strong>R$1.500,00</strong>
                                        <small>ou 10x de R$150,00 sem juros</small>
                                    </div>
                                </a>

                                <a href="#" class="card">
                                    <h5 class="card-title">Assistente Administrativo Fazendário</h5>
                                    <p class="card-text">Matricule-se! Em andamento.</p>

                                    <div class="card-value">
                                        <span>De R$2.000,00</span>
                                        <strong>R$1.500,00</strong>
                                        <small>ou 10x de R$150,00 sem juros</small>
                                    </div>
                                </a>

                                <a href="#" class="card">
                                    <h5 class="card-title">Assistente Administrativo Fazendário</h5>
                                    <p class="card-text">Matricule-se! Em andamento.</p>

                                    <div class="card-value">
                                        <span>De R$2.000,00</span>
                                        <strong>R$1.500,00</strong>
                                        <small>ou 10x de R$150,00 sem juros</small>
                                    </div>
                                </a>

                                <a href="#" class="card">
                                    <h5 class="card-title">Assistente Administrativo Fazendário</h5>
                                    <p class="card-text">Matricule-se! Em andamento.</p>

                                    <div class="card-value">
                                        <span>De R$2.000,00</span>
                                        <strong>R$1.500,00</strong>
                                        <small>ou 10x de R$150,00 sem juros</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endFor; ?>
            </div>
        </div>
    </div>

</section>
