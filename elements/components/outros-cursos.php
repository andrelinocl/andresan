<section data-component="courses" data-layout="default" class="mt-0">
    <div class="container">
        <h2 class="section-title">Outros cursos</h2>
        <p class="section-subtitle">Veja outros cursos semelhantes</p>
        <hr class="xs red">
    </div>


    <div class="container">

        <div class="cards owl-carousel" data-carousel="outros-cursos">
            <?php for ( $i=0; $i<4; $i++ ) : ?>
            <a href="<?php echo BASE_URL . 'curso.php' ?>" class="card pb-3" data-mh="curso">
                <figure class="overlay">
                    <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                </figure>
                <div class="card-body px-3 px-md-5">
                    <div class="card-badge">
                        <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                    </div>
                    <h5 class="card-title">Analista Judiciário</h5>
                    <p class="card-text">Matricule-se! Em andamento.</p>

                    <div class="card-value">
                        <span>De R$2.000,00</span>
                        <strong>R$1.500,00</strong>
                        <small>ou 10x de R$150,00 sem juros</small>
                    </div>
                </div>
                <div class="card-footer px-3 px-md-5">
                    <div class="btn">veja mais detalhes</div>
                </div>
            </a>
            <?php endFor ?>
        </div>
    </div>
</section>
