<section data-component="block-default" style="background-image: url(<?php echo BASE_URL . 'assets/images/pexels-photo-135018.jpeg' ?>);">
    <div class="overlay pink">
        <div class="container text-center">
            <div class="block-title">
                Mais de 30.000 alunos
            </div>
            <div class="block-text">
                Já passaram por nossas salas de aulas - físicas e virtuais
            </div>
            <a href="" class="btn outline white btn-sm">confira todos os depoimentos</a>
        </div>
    </div>
</section>
