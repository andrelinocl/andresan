<section data-component="block-default" style="background-image: url(<?php echo BASE_URL . 'assets/images/' . SECTION_IMAGE ?>);">
    <div class="overlay black small">
        <div class="container text-center">
            <h1 class="h1 text-white">
                <strong><?php echo SECTION_TITLE ?></strong>
            </h1>
            <p class="text-white">
                <?php echo SECTION_SUBTITLE ?>
            </p>
        </div>
    </div>
</section>
