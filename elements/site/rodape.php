<footer>
    <div class="container">
        <div class="row">
            <div class="footer-content">
                <div class="col-links">
                    <h5 class="text-muted">INSTITUCIONAL</h5>
                    <a href="#">Professores</a>
                    <a href="#">Nossas Sedes</a>
                    <a href="#">Depoimentos</a>
                    <a href="#">Contato</a>
                </div>
                <div class="col-links">
                    <h5 class="text-muted">CURSOS</h5>
                    <a href="#">Cursos EAD</a>
                    <a href="#">Cursos Presenciais</a>
                    <a href="#">Cursos de Português</a>
                    <a href="#">Carreiras Policiais</a>
                    <a href="#">Exame da Ordem</a>
                    <a href="#">Cursos Grátis</a>
                    <a href="#">Blog</a>
                </div>
                <div class="col-cards">
                    <h5 class="text-muted">FORMAS DE PAGAMENTOS</h5>
                    <div class="cards">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/master-card.png' ?>" alt="">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/visa.png' ?>" alt="">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/boleto.png' ?>" alt="">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/american-express.png' ?>" alt="">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/bradesco.png' ?>" alt="">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/elo.png' ?>" alt="">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/hiper.png' ?>" alt="">
                        <img src="<?php echo BASE_URL . 'assets/images/cards/hipercard.png' ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-10 offset-sm-1">
                    <p class="text-center">
                        Andresan - Cursos e Concursos - Todos os Direitos Reservados - 2018
                    </p>
                    <a href="https://www.fatordigital.com.br" target="_blank" class="fatordigital">
                        <img src="<?php echo BASE_URL . 'assets/images/fator-digital.png' ?>" alt="Fator Digital - www.fatordigital.com.br">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal-pesquisa">
    <div class="container">

        <a href="javascript:void(0)" data-toggler=".modal-pesquisa|active" class="close"></a>

        <h2 class="modal-title">O que está buscando?</h2>

        <form action="<?php echo BASE_URL . 'resultado-da-busca.php' ?>" method="GET">
            <div class="radio-text">
                <div class="radio-text-content">
                    <input type="radio" name="busca[]" id="curso" checked>
                    <label for="curso" data-placeholder="Digite o nome e/ou assunto do curso">Encontre um curso</label>
                </div>
                <div class="radio-text-content">
                    <input type="radio" name="busca[]" id="site">
                    <label for="site" data-placeholder="Digite o que gostaria de pesquisar no site">Pesquise no site</label>
                </div>
            </div>

            <div class="form-group">
                <input type="text"
                       name="pesquisa"
                       class="form-control"
                       autofocus="autofocus"
                       autocomplete="off"
                >

                <button type="submit" class="btn">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/magnifying-glass.svg' ?>" alt="" class="fd-svg">
                </button>
            </div>
        </form>
    </div>
</div>
