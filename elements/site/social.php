<section data-component="social">
    <div class="container">
        <div class="social-content">
            <p>nos acompanhe nas redes sociais:</p>
            <a href="" class="facebook" title="Facebook">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a href="" class="instagram" title="Instagram">
                <i class="fab fa-instagram"></i>
            </a>
            <a href="" class="google-plus" title="Google+">
                <i class="fab fa-google-plus-g"></i>
            </a>
            <a href="" class="youtube" title="Youtube">
                <i class="fab fa-youtube"></i>
            </a>
        </div>
    </div>
</section>
