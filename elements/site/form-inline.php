<section data-component="form-inline">
    <div class="container">
        <form action="" method="post" class="form-inline" data-validate>
            <p>Receba nossas novidades por e-mail diariamente</p>

            <div class="form-group">
                <input type="email"
                        name="email"
                        placeholder="Digite seu e-mail"
                        class="form-control"
                        required="required"
                        data-rule-required="true"
                        data-msg-required="Preencha o campo para receber nossas novidades"
                        data-rule-email="true"
                        data-msg-email="Preencha com um email válido"
                >
            </div>
            <button type="submit" name="button" class="btn outline white">Cadastre-se</button>
        </form>
    </div>
</section>
