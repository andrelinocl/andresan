<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="pt-br" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo TITLE !== '' ? TITLE.' | ': '';?>Andresan Cursos & Concursos</title>

    <!-- BEIGN: META NAME -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Fator Digital - https://www.fatordigital.com.br">
    <meta name="robots" content="all">
    <meta name="revisit-after" content="5 day">
    <meta name="identifier-url" content="">
    <meta name="language" content="Portuguese" />
    <!-- END: META NAME -->

    <!-- BEGIN: GOOGLE+ TAGS -->
    <link rel="author" href="https://plus.google.com/(Google+_Profile)/posts"/>
    <link rel="publisher" href="https://plus.google.com/(Google+_Page_Profile)"/>
    <meta itemprop="name" content="">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="">
    <!-- END: GOOGLE+ TAGS -->

    <!-- BEGIN: TWITTER CARD -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="Conta do Twitter do site (incluindo arroba)">
    <meta name="twitter:title" content="Título da página">
    <meta name="twitter:description" content="Descrição da página. No máximo 200 caracteres">
    <meta name="twitter:creator" content="Conta do Twitter do autor do texto (incluindo arroba)">
    <!-- imagens largas para o Twitter Summary Card precisam ter pelo menos 280x150px -->
    <meta name="twitter:image" content="http://www.example.com/image.jpg">
    <!-- END: TWITTER CARD -->

    <!-- BEGIN: OG TAGS -->
    <meta property="og:image" content="">
    <meta property="og:site_name" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1260">
    <meta property="og:image:height" content="630">
    <meta property="og:type" content="website">
    <!-- END: OG TAGS -->

    <!-- BEGIN: META TAGS DE GEOLOCALIZACAO -->
    <meta name="geo.region" content="BR-RS" />
    <meta name="geo.placename" content="Porto Alegre" />
    <meta name="geo.position" content="-29.560652;-49.904135" />
    <meta name="ICBM" content="-29.560652, -49.904135" />
    <!-- END: META TAGS DE GEOLOCALIZACAO -->

    <!-- BEGIN: CARREGAMENTO DOS FAVICONS -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo BASE_URL;?>assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo BASE_URL;?>assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo BASE_URL;?>assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo BASE_URL;?>assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASE_URL;?>assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo BASE_URL;?>assets/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-TileImage" content="<?php echo BASE_URL;?>assets/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#000000">
    <!-- END: CARREGAMENTO DOS FAVICONS -->

    <!-- BEGIN: FONTS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,700" rel="stylesheet">
    <!-- END: FONTS -->

    <!-- BEGIN: CSS -->
    <?php echo css('assets/css/lib/lib.min'); ?>
    <?php echo css('assets/css/main.min'); ?>


    <!-- CKEDITOR PARA A AREA DE VIDEO AULA -->
    <script type="text/javascript" src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        var estados = [{"nome":"Acre","sigla":"AC","slug":"acre"},{"nome":"Alagoas","sigla":"AL","slug":"alagoas"},{"nome":"Amapá","sigla":"AP","slug":"amapa"},{"nome":"Amazonas","sigla":"AM","slug":"amazonas"},{"nome":"Bahia","sigla":"BA","slug":"bahia"},{"nome":"Ceará","sigla":"CE","slug":"ceara"},{"nome":"Distrito Federal","sigla":"DF","slug":"distrito federal"},{"nome":"Espírito Santo","sigla":"ES","slug":"espirito santo"},{"nome":"Goiás","sigla":"GO","slug":"goias"},{"nome":"Maranhão","sigla":"MA","slug":"maranhao"},{"nome":"Mato Grosso","sigla":"MT","slug":"mato grosso"},{"nome":"Mato Grosso do Sul","sigla":"MS","slug":"mato grosso do sul"},{"nome":"Minas Gerais","sigla":"MG","slug":"minas gerais"},{"nome":"Pará","sigla":"PA","slug":"para"},{"nome":"Paraíba","sigla":"PB","slug":"paraiba"},{"nome":"Paraná","sigla":"PR","slug":"parana"},{"nome":"Pernambuco","sigla":"PE","slug":"pernambuco"},{"nome":"Piauí","sigla":"PI","slug":"piaui"},{"nome":"Rio de Janeiro","sigla":"RJ","slug":"rio de janeiro"},{"nome":"Rio Grande do Norte","sigla":"RN","slug":"rio grande do norte"},{"nome":"Rio Grande do Sul","sigla":"RS","slug":"rio grande do sul"},{"nome":"Rondônia","sigla":"RO","slug":"rondonia"},{"nome":"Roraima","sigla":"RR","slug":"roraima"},{"nome":"Santa Catarina","sigla":"SC","slug":"santa catarina"},{"nome":"São Paulo","sigla":"SP","slug":"sao paulo"},{"nome":"Sergipe","sigla":"SE","slug":"sergipe"},{"nome":"Tocantins","sigla":"TO","slug":"tocantins"}]
    </script>
    <!-- END: CSS -->
</head>
<body data-page="<?php echo PAGE ?>" data-url="<?php echo BASE_URL ?>">

    <div class="fd-locale">
        <div class="fd-locale__backdrop" data-toggler=".fd-locale|active"></div>
        <div class="fd-locale__content">
            <div class="fd-locale__inner " data-step="1">
                <div class="fd-locale__title">
                    Você está em <br /><span>Porto Alegre-RS</span>?
                </div>
                <div class="fd-locale__buttons">
                    <a href="javascript:void(0)" class="btn btn-red">Sim</a>
                    <a href="javascript:void(0)" class="btn btn-outline" data-next="step-2">Não</a>
                </div>
            </div>
            <div class="fd-locale__inner active" data-step="2">
                <div class="fd-locale__title">
                    Por favor, digite<br />o nome do seu estado:
                </div>
                <form class="fd-locale__form" action="javascript:void(0)" method="post">
                    <div class="mocha__select">
                        <div class="mocha__select-header">
                            <div class="form-group">
                                <input class="mocha__select-input form-control" placeholder="Ex: Rio de Janeiro" autocomplete="off">
                            </div>
                        </div>
                        <ul class="mocha__select-list"></ul>
                    </div>

                    <a href="javascript:geoFindMe();">
                        <img src="<?php echo BASE_URL . 'assets/images/icons/focus.svg' ?>" alt="" class="fd-svg">
                        <span>Usar minha localização</span>
                    </a>
                </form>
            </div>
            <div class="fd-locale__result" id="fd-locale__result"></div>
        </div>
    </div>
    <script>
    function geoFindMe() {
        var output = document.getElementById("fd-locale__result");

        if (!navigator.geolocation){
            output.innerHTML = "<p>A Geolocalização não é suportada pelo seu navegador</p>";
            return;
        }

        function success(position) {
            var latitude  = position.coords.latitude;
            var longitude = position.coords.longitude;

            output.innerHTML = '<p>Latitude is ' + latitude + '° <br>Longitude is ' + longitude + '°</p>';

            var img = new Image();
            img.src = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=300x300&sensor=false&key=YOUR_API_KEY";

            output.appendChild(img);
        }

        function error() {
            output.innerHTML = "Unable to retrieve your location";
        }

        output.innerHTML = "<p>Locating…</p>";

        navigator.geolocation.getCurrentPosition(success, error);
    }
</script>
