<section data-component="menu" class="pb-3">
    <nav class="container">
        <a href="<?php echo BASE_URL ?>" class="brand">
            <img src="<?php echo BASE_URL . 'assets/images/andresan.png' ?>" alt="Andresan Cursos & Concursos">
        </a>

        <a href="tel:08006083030" class="phone d-none d-lg-block">
            <img src="<?php echo BASE_URL . 'assets/images/icons/phone.svg' ?>" alt="" class="fd-svg">
            0800 608 3030
        </a>

        <a href="" class="blog d-none d-lg-block">
            Acesse nosso blog
        </a>

        <div class="posr dropdown-login">
            <a href="#" class="login" data-toggler=".dropdown-login-content|active">
                <figure>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/login.svg'; ?>" alt="" class="fd-svg">
                </figure>
                <span>Login</span>
            </a>
            <div class="dropdown-login-content">
                <div class="h3">Acesse sua conta</div>
                <p class="text-center">Faça seu login e veja seus cursos</p>
                <hr class="xs red">

                <form action="<?php echo BASE_URL . 'area-do-aluno.php' ?>" method="POST" class="validate-login">
                    <div class="form-group">
                        <input type="text" name="usuario" class="form-control" placeholder="Seu e-mail" required data-rule-required="true" data-msg-required="Preencha com seu e-mail" data-rule-email="true" data-msg-email="Preencha com um email válido">
                    </div>
                    <div class="form-group">
                        <input type="text" name="senha" class="form-control" placeholder="Senha" required data-rule-required="true" data-msg-required="Preencha com a sua senha">
                    </div>
                    <div class="d-flex justify-content-between align-center">
                        <label for="rememberme">
                            <input type="checkbox" id="rememberme">
                            Lembrar senha
                        </label>

                        <a href="" class="text-pink">Esqueci minha senha</a>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn py-2 pink btn-block rounded">Acessar minha conta</button>
                    </div>
                </form>

                <p class="text-center my-3">ou</p>

                <button type="submit" class="btn py-2 facebook btn-block rounded">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/facebook.svg' ?>" class="fd-svg">
                    <span>Entrar com o facebook</span>
                </button>
                <button type="submit" class="btn py-2 google btn-block rounded">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/google.svg' ?>" class="fd-svg">
                    <span>Entrar com o Google</span>
                </button>

                <hr class="thin gray-100 my-4">

                <p>Não tem uma conta? <a href="<?php echo BASE_URL . 'cadastro.php' ?>" class="text-pink">Faça seu cadastro</a></p>
            </div>
        </div>


        <a href="<?php echo BASE_URL . 'checkout.php' ?>" class="cart">
            <figure data-badge="5">
                <img src="<?php echo BASE_URL . 'assets/images/icons/cart.svg'; ?>" alt="" class="fd-svg">
            </figure>
            <span>Carrinho</span>
        </a>

        <a href="javascript:void(0)" class="hamb"></a>
    </nav>

    <nav class="menu">
        <div class="close-menu"></div>
        <div class="menu-content">
            <a href="<?php echo BASE_URL . 'cursos.php' ?>" class="active">Cursos EAD</a>
            <a href="<?php echo BASE_URL . 'cursos.php' ?>">Cursos Presenciais</a>
            <a href="<?php echo BASE_URL . 'cursos.php' ?>">Cursos de Português</a>
            <a href="<?php echo BASE_URL . 'cursos.php' ?>">Carreiras Policiais</a>
            <a href="<?php echo BASE_URL . 'cursos.php' ?>">Exame da Ordem</a>
            <a href="<?php echo BASE_URL . 'cursos.php' ?>">Cursos Grátis</a>
            <hr class="xs pink d-md-none">
            <a href="<?php echo BASE_URL . 'institucional.php' ?>" class="d-md-none">Institucional</a>
            <a href="<?php echo BASE_URL . 'cadastro.php' ?>" class="d-md-none">Cadastro</a>
            <a href="<?php echo BASE_URL . 'depoimentos.php' ?>" class="d-md-none">Depoimentos</a>
            <a href="<?php echo BASE_URL . 'contato.php' ?>" class="d-md-none">Contato</a>
        </div>
    </nav>

    <div class="container search-container">
        <a href="javascript:void(0)" class="btn-search" data-toggler=".modal-pesquisa|active">
            <img src="<?php echo BASE_URL . 'assets/images/icons/search.svg' ?>" class="fd-svg">
        </a>
    </div>
</section>
