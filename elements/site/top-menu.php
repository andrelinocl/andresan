<section data-component="top-menu">
    <div class="container">
        <div class="social mr-auto">
            <a href="https://www.facebook.com/andresancursoseconcursos" title="Facebook"><i class="fab fa-facebook-f"></i></a>
            <a href="https://www.instagram.com/andresancursoseconcursos" title="Instagram"><i class="fab fa-instagram"></i></a>
            <a href="https://www.youtube.com/channel/UCLpxSozYH7JdvhnSn-Sqq3w" title="YouTube"><i class="fab fa-youtube"></i></a>
            <a href="https://twitter.com/Curso_Andresan" title="Twitter"><i class="fab fa-twitter"></i></a>
            <a href="javascript:void(0)" class="d-lg-none" data-toggler=".fd-locale|active"><i class="fas fa-map-marker-alt"></i></a>
            <p class="d-none d-lg-flex">Cursos disponíveis para <strong>Porto Alegre - RS</strong> <a href="javascript:void(0)" data-toggler=".fd-locale|active">(alterar)</a></p>
        </div>
        <div class="items ml-auto">
            <a href="<?php echo BASE_URL . 'institucional.php' ?>">Institucional</a>
            <a href="<?php echo BASE_URL . 'cadastro.php' ?>">Cadastro</a>
            <a href="<?php echo BASE_URL . 'depoimentos.php' ?>">Depoimentos</a>
            <a href="<?php echo BASE_URL . 'contato.php' ?>">Contato</a>
        </div>
        <div class="ml-auto search">
            <a href="javascript:void(0)" data-toggler=".modal-pesquisa|active"><i class="fas fa-search"></i></a>
        </div>
    </div>
</section>
