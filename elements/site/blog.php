<section data-components="blog" data-layout="default" class="bg-white">
    <div class="container">

        <h2 class="section-title">Nosso Blog</h2>
        <p class="section-subtitle">acompanhe nossas publicações</p>
        <hr class="xs pink">

        <div class="article-content">
            <div class="row">
                <div class="col-12 col-sm-6 px-1 px-lg-4 px-xl-5">
                    <article class="card">
                        <figure>
                            <img src="<?php echo BASE_URL . 'assets/images/blog-1.jpg' ?>" alt="">
                        </figure>
                        <div class="card-body">
                            <h3 class="card-title" data-mh="article-title">
                                <a href="#" target="_blank">O poder da meditação na vida profissional</a>
                            </h3>
                            <p class="card-text" data-mh="article-text">
                                <a href="#" target="_blank">A meditação pode ser uma importante aliada de sua vida profissional.</a>
                            </p>
                            <a href="" class="btn">ver detalhes</a>
                        </div>
                    </article>
                </div>

                <div class="col-12 col-sm-6 px-1 px-lg-4 px-xl-5">
                    <article class="card">
                        <figure>
                            <img src="<?php echo BASE_URL . 'assets/images/blog-2.jpg' ?>" alt="">
                        </figure>
                        <div class="card-body">
                            <h3 class="card-title" data-mh="article-title">
                                <a href="#" target="_blank">Jungmann confirma: edital do concurso PRF 2018 sai em setembro</a>
                            </h3>
                            <p class="card-text" data-mh="article-text">
                                <a href="#" target="_blank">A meditação pode ser uma importante aliada de sua vida profissional.</a>
                            </p>
                            <a href="" class="btn">ver detalhes</a>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="<?php echo BASE_URL ?>" class="link default">
                confira todas as publicações
            </a>
        </div>
    </div>
</section>
