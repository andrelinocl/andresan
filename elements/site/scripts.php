<script type="text/javascript" src="https://player.vimeo.com/api/player.js"></script>
<script type="text/javascript" src="https://www.youtube.com/iframe_api"></script>


<?php echo js( BASE_URL . 'assets/js/lib/plugins' ); ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php echo js( BASE_URL . 'assets/js/main.min'); ?>

<!-- BAR RATING - USADO NA AVALIAÇÃO DA AULA -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<script type="text/javascript">
$(function() {
    $('#barrating').barrating({
        theme: 'bootstrap-stars',
        initialRating: 0,

        onSelect: function(value, text, event) {
            if (typeof(event) !== 'undefined') {
                // console.log(event.target);

                //console.log($("#barrating").data('current-rating'));
                //console.log($("#barrating").attr('data-current-rating'));

                if($("#barrating").attr('data-current-rating') > 0){

                    // swal({
                    //     title: "VocÃª realmente quer reavaliar a aula?",
                    //     type: 'warning',
                    //     showCancelButton: true,
                    //     confirmButtonColor: '#3085d6',
                    //     cancelButtonColor: '#d33',
                    //     confirmButtonText: 'Sim',
                    //     cancelButtonText: 'NÃ£o'
                    // }).then(function () {
                    //     saveavaliacao(value);
                    //     swal(
                    //         'OK!',
                    //         'Sua avaliaÃ§Ã£o foi salva!',
                    //         'success'
                    //     )
                    // })

                    // if(confirm("VocÃª realmente quer reavaliar a aula?")){
                    //     saveavaliacao(value);
                    // }
                } else {
                    // saveavaliacao(value);
                    // swal(
                    //     'OK!',
                    //     'Sua avaliaÃ§Ã£o foi salva!',
                    //     'success'
                    // )
                }

                $("#barrating").attr('data-current-rating', value);
            }
        }
    });
    $("#barrating").attr('data-current-rating', 4);
    $('#barrating').barrating('set', 4);
});


  var cloudTags = [
    {text: "Justiça", weight: 13, link: 'cursos.php'},
    {text: "depen", weight: 12, link: 'cursos.php'},
    {text: "OAB", weight: 12, link: 'cursos.php'},
    {text: "Inss", weight: 11, link: 'cursos.php'},
    {text: "Polícia Federal", weight: 10, link: 'cursos.php'},
    {text: "Fundatec", weight: 9, link: 'cursos.php'},
    {text: "Rodoviária", weight: 8, link: 'cursos.php'},
    {text: "Ministério", weight: 7, link: 'cursos.php'},
    {text: "Português", weight: 6, link: 'cursos.php'},
    {text: "Sefaz", weight: 5, link: 'cursos.php'},
    {text: "Regional", weight: 4, link: 'cursos.php'},
    {text: "Tribunal", weight: 3, link: 'cursos.php'},
    {text: "Federal", weight: 2, link: 'cursos.php'},
  ];
  $(function() {
    $("#cloud_tags").jQCloud(cloudTags, {shape: "rectangular", autoResize: true});
  });
</script>
