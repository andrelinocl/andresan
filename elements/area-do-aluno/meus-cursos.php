<section data-component="courses" data-layout="default" class="mt-0">

    <div class="section-title">Meus cursos</div>
    <hr class="xs red">

    <div class="container">

        <div class="cards" data-columns="<?php echo COLUMNS ?>" data-grid="grid">

            <a href="<?php echo BASE_URL . 'video-aula-atual.php' ?>" class="card pb-3" data-mh="curso">
                <figure class="overlay">
                    <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                </figure>
                <div class="card-body px-3 px-md-5">
                    <div class="card-badge">
                        <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                    </div>
                    <h5 class="card-title">Analista Judiciário</h5>
                    <p class="card-text mt-3">
                        Aulas assisitdas:
                        <strong class="text-pink">15</strong>/20
                    </p>
                </div>
                <div class="card-footer px-3 px-md-5">
                    <div class="btn mt-2 mx-auto">assistir aulas</div>
                    <p class="mt-3 mt-xl-5">Aulas válidas de 17/08/2017 á 17/10/2017</p>
                </div>
            </a>

            <a href="<?php echo BASE_URL . 'video-aula-flat.php' ?>" class="card pb-3" data-mh="curso">
                <figure class="overlay">
                    <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                </figure>
                <div class="card-body px-3 px-md-5">
                    <div class="card-badge">
                        <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                    </div>
                    <h5 class="card-title">Analista Judiciário</h5>
                    <p class="card-text mt-3">
                        Aulas assisitdas:
                        <strong class="text-pink">15</strong>/20
                    </p>
                </div>
                <div class="card-footer px-3 px-md-5">
                    <div class="btn mt-2 mx-auto">assistir aulas</div>
                    <p class="mt-3 mt-xl-5">Aulas válidas de 17/08/2017 á 17/10/2017</p>
                </div>
            </a>

        </div>
    </div>
</section>
