<section data-component="class-option">
    <div class="container">
        <a href="" class="btn outline gray-900">
            ver outros cursos
        </a>

        <div class="page-title">
            <h1>SEFAZ/RS: <strong>Técnico Tributário</strong></h1>

            <div class="opt">
                <div class="percent">
                    50% <span class="d-none d-md-inline ml-1">assistido</span>
                </div>
                <a href="" class="btn outline gray-900">
                    cronograma
                </a>
                <a href="" class="btn outline gray-900">
                    avisos
                </a>
            </div>
        </div><!-- end [ .PAGE-TITLE ] -->

        <form action="" data-form="search-class">

            <div class="form-group">
                <div class="fd-select-3" data-input="disciplina">
                    <label>Selecione a disciplina</label>
                    <input type="text" name="disciplina">
                    <div class="fd-select-header">
                        <img src="<?php echo BASE_URL . 'assets/images/icons/blackboard.svg' ?>" class="fd-svg">
                        <strong>Disciplina:</strong>
                        <span>Matemática</span>
                        <img src="<?php echo BASE_URL . 'assets/images/icons/down-arrow.svg' ?>" class="fd-svg arrow">
                    </div>
                    <div class="fd-select-body">
                        <input type="text" class="form-control" placeholder="Pesquisa">
                        <div class="fd-select-body-inner">
                            <a href="javascript:void(0)" data-value="matematica">Matemática</a>
                            <a href="javascript:void(0)" data-value="portugues">Português</a>
                            <a href="javascript:void(0)" data-value="trf">TRF</a>
                            <a href="javascript:void(0)" data-value="matematica">Matemática</a>
                            <a href="javascript:void(0)" data-value="portugues">Português</a>
                            <a href="javascript:void(0)" data-value="trf">TRF</a>
                            <a href="javascript:void(0)" data-value="matematica">Matemática</a>
                            <a href="javascript:void(0)" data-value="portugues">Português</a>
                            <a href="javascript:void(0)" data-value="trf">TRF</a>
                            <a href="javascript:void(0)" data-value="matematica">Matemática</a>
                            <a href="javascript:void(0)" data-value="portugues">Português</a>
                            <a href="javascript:void(0)" data-value="trf">TRF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="fd-select-3" data-input="professor">
                    <label>Selecione o professor</label>
                    <input type="text" name="professor">
                    <div class="fd-select-header">
                        <img src="<?php echo BASE_URL . 'assets/images/icons/man-user.svg' ?>" class="fd-svg">
                        <strong>Professor:</strong>
                        <span>João Carlos</span>
                        <img src="<?php echo BASE_URL . 'assets/images/icons/down-arrow.svg' ?>" class="fd-svg arrow">
                    </div>
                    <div class="fd-select-body">
                        <input type="text" class="form-control" placeholder="Pesquisa">
                        <div class="fd-select-body-inner">
                            <a href="javascript:void(0)" data-value="joao carlos">João Carlos</a>
                            <a href="javascript:void(0)" data-value="andresan machado">Andresan Machado</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="fd-select-3" data-input="assunto">
                    <label>Selecione o assunto</label>
                    <input type="text" name="assunto">
                    <div class="fd-select-header">
                        <img src="<?php echo BASE_URL . 'assets/images/icons/book.svg' ?>" class="fd-svg">
                        <strong>Assunto:</strong>
                        <span>Orações Subordinadas</span>
                        <img src="<?php echo BASE_URL . 'assets/images/icons/down-arrow.svg' ?>" class="fd-svg arrow">
                    </div>
                    <div class="fd-select-body">
                        <input type="text" class="form-control" placeholder="Pesquisa">
                        <div class="fd-select-body-inner">
                            <a href="javascript:void(0)" data-value="os i">Orações Subordinadas I</a>
                            <a href="javascript:void(0)" data-value="os ii">Orações Subordinadas II</a>
                            <a href="javascript:void(0)" data-value="os iii">Orações Subordinadas III</a>
                            <a href="javascript:void(0)" data-value="os iv">Orações Subordinadas IV</a>
                            <a href="javascript:void(0)" data-value="os v">Orações Subordinadas V</a>
                            <a href="javascript:void(0)" data-value="os vi">Orações Subordinadas VI</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="fd-select-3" data-input="aula">
                    <label>Selecione a aula</label>
                    <input type="text" name="aula">
                    <div class="fd-select-header">
                        <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                        <strong>Aula 01:</strong>
                        <span>Apresentação do curso</span>
                        <img src="<?php echo BASE_URL . 'assets/images/icons/down-arrow.svg' ?>" class="fd-svg arrow">
                    </div>
                    <div class="fd-select-body">
                        <input type="text" class="form-control" placeholder="Pesquisa">
                        <div class="fd-select-body-inner">
                            <a href="javascript:void(0)" data-value="Orações Subordinadas">Apresentação do curso</a>
                            <a href="javascript:void(0)" data-value="Orações Subordinadas">Orações Subordinadas II</a>
                            <a href="javascript:void(0)" data-value="Orações Subordinadas">Orações Subordinadas III</a>
                            <a href="javascript:void(0)" data-value="Orações Subordinadas">Orações Subordinadas IV</a>
                            <a href="javascript:void(0)" data-value="Orações Subordinadas">Orações Subordinadas V</a>
                            <a href="javascript:void(0)" data-value="Orações Subordinadas">Orações Subordinadas VI</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
