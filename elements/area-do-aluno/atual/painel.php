<section data-component="painel">
    <div class="container py-5">
        <h3 class="h2">SEFAZ/RS: <strong>Técnico Tributário</strong></h3>

        <div class="row-materiais">
            <button class="btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseMateriais" aria-expanded="false" aria-controls="collapseMateriais">
                <!-- o elemento <i> cria o ícone de mais e menos -->
                <i></i>
                <img src="<?php echo BASE_URL . 'assets/images/icons/open-folder.svg' ?>" class="fd-svg">
                Painel de Materiais por Disciplinas
            </button>

            <div class="fd-select-3" data-input="disciplina">
                <label>Selecione a disciplina</label>
                <input type="text" name="disciplina">
                <div class="fd-select-header">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/man-user.svg' ?>" class="fd-svg">
                    <strong>Disciplina:</strong>
                    <span>Matemática</span>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/down-arrow.svg' ?>" class="fd-svg arrow">
                </div>
                <div class="fd-select-body">
                    <input type="text" class="form-control" placeholder="Pesquisa">
                    <div class="fd-select-body-inner">
                        <a href="javascript:void(0)" data-value="matematica">Matemática</a>
                        <a href="javascript:void(0)" data-value="portugues">Português</a>
                    </div>
                </div>
            </div><!-- END [ .FD-SELECT-3 ] -->

            <a href="javascript:void(0)" class="btn pink">Compre a apostila</a>
        </div>
        <div class="collapse" id="collapseMateriais">
            <div>
                <div class="fd-table">
                    <div class="fd-row">
                        <div class="fd-name">
                            <strong>Aula 01:</strong> Gramatica para concursos
                        </div>
                        <div class="fd-options">
                            <span class="text-pink">22:05</span>
                            <a href="" class="btn">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/google-drive-pdf-file.svg' ?>" class="fd-svg">
                                <span class="d-none d-md-inline">Material</span>
                            </a>
                            <a href="" class="btn">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/windows.svg' ?>" class="fd-svg">
                                <span class="d-none d-md-inline">Slides</span>
                            </a>
                            <a href="" class="btn">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/speaker-filled-audio-tool.svg' ?>" class="fd-svg">
                                <span class="d-none d-md-inline">Áudio</span>
                            </a>
                            <a href="" class="btn">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/eye.svg' ?>" class="fd-svg">
                                <span class="d-none d-sm-inline">Assisitdo 5 vezes <strong class="text-red">100%</strong>
                                </span>
                            </a>

                            <a href="" class="btn">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                                <span class="d-none d-lg-inline">Assistir novamente
                                </span>
                            </a>

                        </div>
                    </div><!-- END [ .FD-ROW ] -->
                </div>
            </div>
        </div>

        <hr class="thin gray-100">


        <div class="row-disciplina">
            <button class="btn btn-collapse" type="button" data-toggle="collapse" data-target="#collapseVideoaulas" aria-expanded="false" aria-controls="collapseVideoaulas">
                <!-- o elemento <i> cria o ícone de mais e menos -->
                <i></i>
                <img src="<?php echo BASE_URL . 'assets/images/icons/open-folder.svg' ?>" class="fd-svg">
                Painel de Videoaulas
            </button>

            <div class="fd-select-3" data-input="disciplina">
                <label>Selecione a disciplina</label>
                <input type="text" name="disciplina">
                <div class="fd-select-header">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/man-user.svg' ?>" class="fd-svg">
                    <span>Matemática</span>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/down-arrow.svg' ?>" class="fd-svg arrow">
                </div>
                <div class="fd-select-body">
                    <input type="text" class="form-control" placeholder="Pesquisa">
                    <div class="fd-select-body-inner">
                        <a href="javascript:void(0)" data-value="matematica">Matemática</a>
                        <a href="javascript:void(0)" data-value="portugues">Português</a>
                    </div>
                </div>
            </div><!-- END [ .FD-SELECT-3 ] -->

            <div class="fd-select-3" data-input="professor">
                <label>Selecione o Professor</label>
                <input type="text" name="professor">
                <div class="fd-select-header">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/man-user.svg' ?>" class="fd-svg">
                    <span>Fernando Aprado</span>
                    <img src="<?php echo BASE_URL . 'assets/images/icons/down-arrow.svg' ?>" class="fd-svg arrow">
                </div>
                <div class="fd-select-body">
                    <input type="text" class="form-control" placeholder="Pesquisa">
                    <div class="fd-select-body-inner">
                        <a href="javascript:void(0)" data-value="fernando aprado">Fernando Aprado</a>
                        <a href="javascript:void(0)" data-value="andresan machado">Andresan Machado</a>
                    </div>
                </div>
            </div><!-- END [ .FD-SELECT-3 ] -->

            <a href="javascript:void(0)" class="btn pink">
                <i class="far fa-heart"></i>
                Visualizar Favoritos
            </a>
        </div>
        <div class="collapse show" id="collapseVideoaulas">
            <div class="fd-table">
                <div class="fd-row">
                    <div class="fd-name">
                        <strong>Aula 01:</strong> Gramatica para concursos
                    </div>
                    <div class="fd-options">
                        <span class="text-pink">22:05</span>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/google-drive-pdf-file.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Material</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/windows.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Slides</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/speaker-filled-audio-tool.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Áudio</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/eye.svg' ?>" class="fd-svg">
                            <span class="d-none d-sm-inline">Assisitdo 5 vezes <strong class="text-red">100%</strong>
                            </span>
                        </a>

                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                            <span class="d-none d-lg-inline">Assistir novamente
                            </span>
                        </a>

                    </div>
                </div><!-- END [ .FD-ROW ] -->

                <div class="fd-row">
                    <div class="fd-name">
                        <strong>Aula 02:</strong> Sujeito, verbo e predicado
                    </div>
                    <div class="fd-options">
                        <span class="text-pink">22:05</span>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/google-drive-pdf-file.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Material</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/windows.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Slides</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/speaker-filled-audio-tool.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Áudio</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/eye.svg' ?>" class="fd-svg">
                            <span class="d-none d-sm-inline">Assisitdo 0 vezes <strong class="text-red">0%</strong>
                            </span>
                        </a>

                        <a href="" class="btn pink">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                            <span class="d-none d-lg-inline">Assistir videoaula
                            </span>
                        </a>

                    </div>
                </div><!-- END [ .FD-ROW ] -->

                <div class="fd-row">
                    <div class="fd-name">
                        <strong>Aula 02:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit
                    </div>
                    <div class="fd-options">
                        <span class="text-pink">22:05</span>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/google-drive-pdf-file.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Material</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/windows.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Slides</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/speaker-filled-audio-tool.svg' ?>" class="fd-svg">
                            <span class="d-none d-md-inline">Áudio</span>
                        </a>
                        <a href="" class="btn">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/eye.svg' ?>" class="fd-svg">
                            <span class="d-none d-sm-inline">Assisitdo 0 vezes <strong class="text-red">0%</strong>
                            </span>
                        </a>

                        <a href="" class="btn pink">
                            <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                            <span class="d-none d-lg-inline">Assistir videoaula
                            </span>
                        </a>

                    </div>
                </div><!-- END [ .FD-ROW ] -->
            </div><!-- END [ .FD-TABLE ] -->
        </div>
    </div>
</section>
