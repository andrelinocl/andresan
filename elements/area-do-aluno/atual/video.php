<section data-component="video" class="py-5">
    <div class="container video-title">
        <p>
            <span class="text-pink">Aula 1</span> Apresentação do curso
            <small>Assistido <img src="<?php echo BASE_URL . 'assets/images/icons/tick.svg' ?>" class="fd-svg"></small>
        </p>
        <div class="buttons">
            <a href="#" class="btn gray-100">
                <img src="<?php echo BASE_URL . 'assets/images/icons/windows.svg' ?>" class="fd-svg">
                <span>ver slides da aula</span>
            </a>
            <a href="#" class="btn outline gray-900">
                <img src="<?php echo BASE_URL . 'assets/images/icons/left-arrow.svg' ?>" class="fd-svg">
                <span>aula anterior</span>
            </a>
            <a href="#" class="btn pink">
                <span>próxima aula</span>
                <img src="<?php echo BASE_URL . 'assets/images/icons/right-arrow.svg' ?>" class="fd-svg">
            </a>
        </div><!-- END [ .BUTTONS ] -->
    </div><!-- END [ .VIDEO-TITLE ] -->


    <div class="video-section">
        <p>
            Alterar a velocidade <strong>Aceleração do video 1.0x</strong>
        </p>

        <div class="row-video">


            <div class="row-video__content" data-width="66">
                <?php elements('area-do-aluno/overlay-info'); ?>
                <?php elements('area-do-aluno/overlay-next'); ?>
                <?php elements('area-do-aluno/video-mask'); ?>

                <div class="embed-responsive embed-responsive-16by9">
                    <!-- VIMEO -->
                    <iframe id="embed-responsive__iframe" class="embed-responsive-item" data-type="vimeo" src="https://player.vimeo.com/video/313878877" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                    <!-- YOUTUBE -->
                    <!-- <iframe id="embed-responsive__iframe" type="text/html" src="https://www.youtube.com/embed/Z7CTnA3dTU0?rel=1&controls=1&showinfo=0&enablejsapi=1&origin=http://andresan-fe.oo" frameborder="0" allowfullscreen data-type="youtube"></iframe> -->
                    <!-- <div id="embed-responsive__iframe" data-type="youtube" data-video-id="Z7CTnA3dTU0"></div> -->
                </div>
            </div>

            <aside class="sidebar-video" data-width="33">
                <img src="<?php echo BASE_URL . 'assets/images/andresan.png' ?>" alt="" class="aside-logo">
                <p class="text-center">Faça aqui suas anotações</p>
                <form class="" action="javascript:void()" method="post">
                    <textarea name="editor1"></textarea>
                </form>
            </aside>
            <script>
            CKEDITOR.config.height = 500;
            CKEDITOR.config.toolbar = [
                ['Format','Font','FontSize'],
                '/',
                ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
                '/',
                ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
            ] ;
            CKEDITOR.disableAutoInline = true;
            CKEDITOR.inline( 'editor1' );
            </script>
        </div><!-- END [ .ROW-VIDEO ] -->
    </div><!-- END [ .VIDEO-SECTION ] -->


    <div class="video-options">
        <div class="d-sm-flex">
            <div class="video-rating">
                Avalie a aula:
                <select id="barrating" data-current-rating="0">
                    <option value=""></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div><!-- END [ VIDEO-RATING ] -->
            <div class="video-comment">
                <div class="dropdown">
                    <button class="btn white text-pink dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Envie seu comentário
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <label class="px-2">Deixe seu comentário abaixo</label>
                        <form class="p-3" action="javascript:void(0)" method="post" data-validate>
                            <div class="form-group">
                                <textarea name="comment" class="form-control" id="" cols="30" rows="10" required="required" data-rule-required="true" data-msg-required="Preencha o campo com seu comentário"></textarea>
                            </div>
                            <button type="submit" class="btn btn-block pink">Enviar</button>
                        </form>
                    </div><!-- END [ .DROPDOWN-MENU ] -->
                </div><!-- END [ .DROPDOWN ] -->
            </div><!-- END [ .VIDEO-COMMENT ] -->
        </div>


        <div class="d-flex">
            <div class="change-favs">
                <a href="javascript:void(0)" class="btn outline gray-100 text-gray-900">
                    <i class="far fa-heart"></i>
                    <i class="fas fa-heart d-none"></i>
                </a>
            </div><!-- END [ .CHANGE-FAVS ] -->
            <div class="change-light">
                <a href="javascript:void(0)" class="btn outline gray-100 text-gray-900">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/light-bulb2.svg' ?>" class="fd-svg">
                </a>
            </div><!-- END [ .CHANGE-LIGHT ] -->
            <div class="change-size">
                <span>Opções de visualização</span>
                <a href="javascript:void(0)" data-width="66|33" class="selected">
                    <img src="<?php echo BASE_URL . 'assets/images/png/w-2-3.png' ?>" alt="">
                </a>
                <a href="javascript:void(0)" data-width="33|66">
                    <img src="<?php echo BASE_URL . 'assets/images/png/w-1-3.png' ?>" alt="">
                </a>
                <a href="javascript:void(0)" data-width="50|50">
                    <img src="<?php echo BASE_URL . 'assets/images/png/w-1-1.png' ?>" alt="">
                </a>
                <a href="javascript:void(0)" data-width="100|100">
                    <img src="<?php echo BASE_URL . 'assets/images/png/w-100-100.png' ?>" alt="">
                </a>
            </div><!-- END [ .CHANGE-SIZE ] -->
        </div>
    </div><!-- END [ .VIDEO-OPTIONS ] -->
</section>
