<!-- <div class="main-overlay-next" style="background-image: url(<?php echo BASE_URL . 'assets/images/ao-vivo.jpg' ?>)"> -->
<div class="main-overlay-next">
    <div class="main-overlay-next__inner">

        <a href="javascript:void(0)" class="main-overlay-next__again" style="background-image: url(<?php echo BASE_URL . 'assets/images/banner-curso.jpg' ?>)">
            <img src="<?php echo BASE_URL . 'assets/images/icons/repeat.svg' ?>" alt="" class="fd-svg">
            <span class="mt-2">Assistir aula novamente</span>
        </a>

        <div class="main-overlay-next__content">

            <div class="main-overlay-next__text">
                <p>
                    Curso Português Completo
                    <small>Aposto; Vocativo; Predicativo</small>
                    <strong>INSS - Técnico do Seguro Social - EAD Propulsor | Parte II</strong>
                </p>
                <a href="#" data-toggler=".main-overlay-info|active" class="btn">Veja todas as partes desta aula</a>
            </div>

            <div class="main-overlay-next__figure">
                <span>Próximo vídeo desta aula:</span>
                <a href="javascript:void(0)" class="main-overlay-next__image" style="background-image: url(<?php echo BASE_URL . 'assets/images/banner-curso.jpg' ?>)">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/play-button-bold.svg' ?>" alt="" class="fd-svg">
                </a>
            </div>

        </div>
    </div>
</div>
