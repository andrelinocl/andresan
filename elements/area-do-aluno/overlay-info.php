<div class="main-overlay-info active">
    <div class="cards">
        <a href="javascript:void(0)" class="card active playAgain">
            <div class="card-header" style="background-image: url(<?php echo BASE_URL . 'assets/images/rawpixel.jpg' ?>)">
                <img src="<?php echo BASE_URL . 'assets/images/icons/repeat.svg' ?>" alt="" class="fd-svg">
                <span class="mt-2">Assistir aula novamente</span>
            </div>
            <div class="card-footer">
                <p>
                    <span>Aposto, Vocativo e Predicativo</span>
                    INSS - Técnico do Seguro Social - EAD Propulsor | Parte I
                </p>
            </div>
        </a>
        <a href="javascript:playNext()" class="card">
            <div class="card-header" style="background-image: url(<?php echo BASE_URL . 'assets/images/rawpixel.jpg' ?>)">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play-button-bold.svg' ?>" alt="" class="fd-svg">
            </div>
            <div class="card-footer">
                <p>
                    <span>Aposto, Vocativo e Predicativo</span>
                    INSS - Técnico do Seguro Social - EAD Propulsor | Parte II
                </p>
            </div>
        </a>
        <a href="" class="card">
            <div class="card-header" style="background-image: url(<?php echo BASE_URL . 'assets/images/rawpixel.jpg' ?>)">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play-button-bold.svg' ?>" alt="" class="fd-svg">
            </div>
            <div class="card-footer">
                <p>
                    <span>Aposto, Vocativo e Predicativo</span>
                    INSS - Técnico do Seguro Social - EAD Propulsor | Parte III
                </p>
            </div>
        </a>
        <a href="" class="card">
            <div class="card-header" style="background-image: url(<?php echo BASE_URL . 'assets/images/rawpixel.jpg' ?>)">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play-button-bold.svg' ?>" alt="" class="fd-svg">
            </div>
            <div class="card-footer">
                <p>
                    <span>Aposto, Vocativo e Predicativo</span>
                    INSS - Técnico do Seguro Social - EAD Propulsor | Parte III
                </p>
            </div>
        </a>
    </div>
</div>
<div class="main-overlay__backdrop"></div>
