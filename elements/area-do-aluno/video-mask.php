<?php
$videoMaskTitle = 'INSS - Técnico do Seguro Social';
?>

<div class="video-mask" id="video-mask__button" data-video-player="vimeo">
    <div class="video-mask__upper">
        <div class="video-mask__brand">
            <img src="<?php echo BASE_URL . 'assets/images/andresan-brand-red.png' ?>" alt="Andresan">
        </div>
        <div class="video-mask__title <?php echo strlen($videoMaskTitle) > 40 ? 'smaller' : 'larger' ; ?>">
            <?php echo $videoMaskTitle; ?>
        </div>
        <div class="video-mask__play-button">
            <img src="<?php echo BASE_URL . 'assets/images/play-button.png' ?>" alt="Iniciar Video Aula" class="img-fluid">
        </div>
    </div>
    <div class="video-mask__under">
        <div class="video-mask__description">
            Aula 01 - A<br />
            Raciocínio Lógico 01
            <span>Preposições, Conetivos e Tabela verdade</span>
        </div>
    </div>
</div>
