<?php
include 'config.php';
define('TITLE', 'Contato');
define('PAGE', 'contato');
define('SECTION_TITLE', 'Contato');
define('SECTION_SUBTITLE', 'Utilize o formulário abaixo e entre em contato');
define('SECTION_IMAGE', 'cadastro.jpg');

elements('site/header');

elements('site/top-menu');
elements('site/menu');
elements('blocks/default');
?>

<section data-component="contato">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 col-sm-5">
                <form class="" action="javascript:void(0)" method="post" data-validate>
                    <div class="form-group">
                        <input type="text"
                                name="nome"
                                placeholder="Nome"
                                class="form-control"
                                required="required"
                                data-rule-required="true"
                                data-msg-required="Preenchimento obrigatório"
                                >
                    </div>

                    <div class="form-group">
                        <input type="email"
                                name="email"
                                placeholder="E-mail"
                                class="form-control"
                                required="required"
                                data-rule-required="true"
                                data-msg-required="Preenchimento obrigatório"
                                data-rule-email="true"
                                data-msg-email="Preencha com um email válido"
                                >
                    </div>

                    <div class="form-group">
                        <input type="text"
                                name="assunto"
                                placeholder="Assunto"
                                class="form-control"
                                required="required"
                                data-rule-required="true"
                                data-msg-required="Preenchimento obrigatório"
                                >
                    </div>

                    <div class="form-group">
                        <textarea name="name" class="form-control" placeholder="Mensagem" required="required" data-rule-required="true" data-msg-required="Preenchimento obrigatório"></textarea>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn pink">enviar mensagem</button>
                    </div>
                </form>
            </div>
            <div class="col-12 offset-sm-1 col-sm-6">
                <div class="sedes">
                    <h4 class="text-pink">Sede Centro</h4>
                    <address>
                        Rua Doutor Flores, 327/301 - Centro Histórico, Porto Alegre/RS
                    </address>
                    <p class="mb-5">
                        Horário de Atendimento:
                        <span><strong>Segunda a Sexta:</strong> 8h às 22h15min</span>

                        <span><strong>Sábados:</strong> 8h às 16h</span>

                        <span class="text-pink mt-3">
                            <strong class="d-block">Telefone</strong>
                            <a href="tel:5130309999">(51) 3030-9999</a>
                        </span>
                    </p>
                    <h4 class="text-pink">Sede Centro</h4>
                    <address>
                        Rua Doutor Flores, 327/301 - Centro Histórico, Porto Alegre/RS
                    </address>
                    <p class="mb-5">
                        Horário de Atendimento:
                        <span><strong>Segunda a Sexta:</strong> 8h às 22h15min</span>

                        <span><strong>Sábados:</strong> 8h às 16h</span>

                        <span class="text-pink mt-3">
                            <strong class="d-block">Telefone</strong>
                            <a href="tel:5130309999">(51) 3030-9999</a>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
elements('site/social');

elements('site/form-inline');

elements('site/rodape');
elements('site/scripts');
elements('site/footer');
?>
