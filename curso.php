<?php
include 'config.php';
define('TITLE', 'Curso Analista Judiciário');
define('PAGE', 'curso');

elements('site/header');

elements('site/top-menu');
elements('site/menu');
//

elements('curso/info2');
elements('curso/banner');
elements('curso/info');
elements('curso/conteudo');
elements('components/video-apresentacao');
elements('components/form-duvida');
elements('components/outros-cursos');

elements('site/social');
elements('site/form-inline');
elements('site/rodape');
elements('site/scripts');
elements('site/footer');

?>
