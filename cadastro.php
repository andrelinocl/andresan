<?php
include 'config.php';
define('TITLE', 'Cadastro');
define('PAGE', 'cadastro');
define('SECTION_TITLE', 'Cadastro');
define('SECTION_SUBTITLE', 'Preencha todos os campos e efetue seu cadastro');
define('SECTION_IMAGE', 'cadastro.jpg');

elements('site/header');

elements('site/top-menu');
elements('site/menu');
elements('blocks/default');
?>

<section data-component="cadastro" class="py-5">
    <form class="form-cadastro" action="javascript:void(0)" method="post" data-validate data-modal=".cadastro-modal">
        <div class="container">
            <h5 class="h5 mt-3 text-gray">Dados Pessoais</h5>
            <div class="form-content">
                <div class="form-group">
                    <input type="text"
                            name="nome"
                            placeholder="Nome"
                            class="form-control"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="text"
                            name="sobrenome"
                            placeholder="Sobrenome"
                            class="form-control"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="text"
                            name="cpf"
                            placeholder="CPF"
                            class="form-control"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            data-rule-minlength="14"
                            data-msg-minlength="Preencha corretamente"
                            data-mask="cpf"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="text"
                            name="data-nascimento"
                            placeholder="Data de Nascimento"
                            class="form-control"
                            required="required"
                            data-mask="nascimento"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="text"
                            name="escolaridade"
                            placeholder="Escolaridade"
                            class="form-control"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->

                <div class="form-group">
                    <select class="form-control" name="sexo">
                        <option value="">Selecione um sexo</option>
                        <option value="Feminino">Feminino</option>
                        <option value="Masculino">Masculino</option>
                    </select>
                </div><!-- END [ .FORM-GROUP ] -->
            </div><!-- END [ .FORM-CONTENT ] -->

            <h5 class="h5 mt-3 text-gray">Contato</h5>
            <div class="form-content">
                <div class="form-group">
                    <input type="text"
                            name="email"
                            placeholder="Email"
                            class="form-control"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            data-rule-email="true"
                            data-msg-email="Preencha o campo corretamente"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="text"
                            name="telefone"
                            placeholder="Telefone"
                            class="form-control"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            data-rule-minlength="14"
                            data-msg-minlength="Preencha o campo corretamente"
                            data-mask="phone"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
            </div><!-- END [ .FORM-CONTENT ] -->

            <h5 class="h5 mt-3 text-gray">Endereço</h5>
            <div class="form-content">
                <div class="form-group">
                    <input type="text"
                            name="cep"
                            placeholder="Informe o CEP"
                            class="form-control cep"
                            required="required"
                            data-mask="cep"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="text"
                            name="estado"
                            placeholder="Informe o Estado"
                            class="form-control estado"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >

                    <!-- <select class="form-control" name="estado">
                        <option value="">Estado</option>
                        <option value="RS">Rio Grande do Sul</option>
                        <option value="SC">Santa Catarina</option>
                    </select> -->
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="text"
                            name="cidade"
                            placeholder="Cidade"
                            class="form-control cidade"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group ">
                    <input type="text"
                            name="bairro"
                            placeholder="Bairro"
                            class="form-control bairro"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group ">
                    <input type="text"
                            name="endereco"
                            placeholder="Endereço"
                            class="form-control endereco"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group w-25">
                    <input type="text"
                            name="numero"
                            placeholder="Número"
                            class="form-control numero"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group w-25">
                    <input type="text"
                            name="complemento"
                            placeholder="Complemento"
                            class="form-control"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
            </div><!-- END [ .FORM-CONTENT ] -->

            <h5 class="h5 mt-3 text-pink">Dados de Acesso</h5>
            <div class="form-content">
                <div class="form-group">
                    <input type="text"
                            name="usuario"
                            placeholder="Nome para login"
                            class="form-control error"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
                <div class="form-group">
                    <input type="password"
                            name="senha"
                            placeholder="Senha"
                            class="form-control error"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preenchimento obrigatório"
                            >
                </div><!-- END [ .FORM-GROUP ] -->
            </div><!-- END [ .FORM-CONTENT ] -->
        </div><!-- END [ .CONTAINER ] -->

        <div class="text-center mt-4">
            <button type="submit" class="btn pink">Cadastrar dados</button>
        </div>
    </form>
</section>

<div class="cadastro-modal">
    <div class="backdrop" data-toggler=".cadastro-modal|active"></div>
    <main>
        <a href="javascript:void(0)" class="close-modal" data-toggler=".cadastro-modal|active">
            <img src="<?php echo BASE_URL . 'assets/images/icons/close-button.svg' ?>" class="fd-svg">
        </a>
        <figure>
            <img src="<?php echo BASE_URL . 'assets/images/icons/tick.svg' ?>" class="fd-svg">
        </figure>
        <div class="h3 text-center">
            Cadastro confirmado
        </div>
        <p class="text-center">
            Seu cadastro foi realizado com sucesso
        </p>
        <div class="modal-footer">
            <a href="javascript:void(0)" data-toggler=".cadastro-modal|active" class="btn outline gray-900">continuar</a>
            <a href="<?php echo BASE_URL . 'area-do-aluno.php' ?>" class="btn pink">ir para a área do aluno</a>
        </div>
    </main>
</div>

<?php
elements('site/social');

elements('site/form-inline');

elements('site/rodape');
elements('site/scripts');
elements('site/footer');
?>
