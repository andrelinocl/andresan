<?php
include 'config.php';
define('TITLE', '');
define('PAGE', 'cursos');
define('COLUMNS', 4);

elements('site/header');

elements('site/top-menu');
elements('site/menu');

?>

<section data-layout="default">
    <div class="container">
        <h3 class="h3 text-center mb-0">Resultado de sua busca</h3>
        <p class="text-center">confira o que encontramos</p>
        <hr class="xs red">

        <form class="form-pesquisa" action="" method="post">
            <div class="form-group">
                <input type="text"
                class="form-control"
                placeholder="Digite o que gostaria de pesquisar no site"
                value="Rio Grande do Sul"
                >

                <button type="submit" class="btn">
                    <img src="<?php echo BASE_URL . 'assets/images/icons/magnifying-glass.svg' ?>" alt="" class="fd-svg">
                </button>
            </div>
        </form>
        <hr class="thin gray-100 my-5">
    </div>
    <section data-component="courses">
        <div class="container">
            <div class="cards" data-columns="<?php echo COLUMNS ?>" data-grid="grid">
                <?php for ( $i=0; $i<8; $i++ ) : ?>
                    <a href="<?php echo BASE_URL . 'curso.php' ?>" class="card" data-mh="curso">
                        <figure class="overlay">
                            <img class="card-img-top" src="<?php echo BASE_URL . 'assets/images/pexels-photo-1308783.jpeg' ?>" alt="Card image cap">
                        </figure>
                        <div class="card-body px-3 px-md-5">
                            <div class="card-badge">
                                <img src="<?php echo BASE_URL . 'assets/images/badge-rs.png' ?>" alt="">
                            </div>
                            <h5 class="card-title">Analista Judiciário</h5>
                            <p class="card-text">Matricule-se! Em andamento.</p>

                            <div class="card-value">
                                <span>De R$2.000,00</span>
                                <strong>R$1.500,00</strong>
                                <small>ou 10x de R$150,00 sem juros</small>
                            </div>
                        </div>
                        <div class="card-footer px-3 px-md-5">
                            <div class="btn">veja mais detalhes</div>
                        </div>
                    </a>
                    <?php endFor ?>
                </div>
            </div>
        </section>

    </section>



    <?php

    elements('site/form-inline');

    elements('site/rodape');
    elements('site/scripts');
    elements('site/footer');
    ?>
