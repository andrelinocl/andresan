<?php
include 'config.php';
define('TITLE', '');
define('PAGE', 'area-do-aluno');
define('COLUMNS', 3);

elements('site/header');

elements('site/top-menu');
elements('site/menu');

elements('area-do-aluno/meus-cursos');

elements('site/form-inline');
elements('site/rodape');
elements('site/scripts');
elements('site/footer');
?>
