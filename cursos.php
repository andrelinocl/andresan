<?php
include 'config.php';
define('TITLE', '');
define('PAGE', 'cursos');
define('SECTION_TITLE', 'Nossos cursos');
define('SECTION_SUBTITLE', 'Conheça todos os cursos disponíveis');
define('COLUMNS', 4);
define('FILTER', true);

elements('site/header');

elements('site/top-menu');
elements('site/menu');
?>
<section data-layout="default">
<?php
elements('components/cursos-home');
?>
</section>
<?php


elements('site/form-inline');

elements('site/rodape');
elements('site/scripts');
elements('site/footer');
?>
