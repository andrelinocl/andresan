<?php
include 'config.php';

define('TITLE', '');
define('PAGE', 'home');

elements('site/header');
?>

<section data-component="checkout">

    <div class="container text-center mb-5">
        <img src="<?php echo BASE_URL . 'assets/images/andresan2.png' ?>" alt="Andresan Cursos e Concursos" class="img-fluid">
    </div>

    <form action="">
        <div class="container">
            <div class="owl-carousel" data-carousel="checkout">
                <div class="item step-1" id="step-1">
                    <header>
                        <span>1</span> Curso Selecionado
                    </header>
                    <main>
                        <div class="curso">
                            <div class="curso-info">
                                <input type="text" name="curso-1" value="curso-1" required>
                                <p>
                                    <strong class="text-pink">Analista Judicário</strong>
                                    R$9.999,99<br />
                                    <span class="text-pink">Desconto <small>(CUPOM)</small> - R$ 0,00</span>
                                </p>
                            </div>
                            <div class="curso-remove" data-target=".curso">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/trash.svg' ?>" class="fd-svg">
                                <span>REMOVER</span>
                            </div>
                        </div>
                        <div class="curso">
                            <div class="curso-info">
                                <input type="text" name="curso-1" value="curso-1">
                                <p>
                                    <strong class="text-pink">Analista Judicário</strong>
                                    R$9.999,99<br />
                                    <span class="text-pink">Desconto <small>(CUPOM)</small> - R$ 0,00</span>
                                </p>
                            </div>
                            <div class="curso-remove" data-target=".curso">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/trash.svg' ?>" class="fd-svg">
                                <span>REMOVER</span>
                            </div>
                        </div>
                        <div class="curso">
                            <div class="curso-info">
                                <input type="text" name="curso-1" value="">
                                <p>
                                    <strong class="text-pink">Analista Judicário</strong>
                                    R$9.999,99<br />
                                    <span class="text-pink">Desconto <small>(CUPOM)</small> - R$ 0,00</span>
                                </p>
                            </div>
                            <div class="curso-remove" data-target=".curso">
                                <img src="<?php echo BASE_URL . 'assets/images/icons/trash.svg' ?>" class="fd-svg">
                                <span>REMOVER</span>
                            </div>
                        </div>
                    </main>

                    <div class="form-inline">
                        <input type="text" class="form-control" name="" placeholder="CÓDIGO">
                        <a href="" class="btn">Validar</a>
                    </div>

                    <div class="table-flex">
                        <div class="table-row">
                            <div>Subtotal</div>
                            <div>R$9.999,00</div>
                        </div>
                        <div class="table-row">
                            <div class="text-pink">Desconto <small>(CUPOM)</small></div>
                            <div class="text-pink">R$9.999,00</div>
                        </div>
                        <div class="table-row mt-5">
                            <div>Total</div>
                            <div>R$9.999,00</div>
                        </div>
                    </div>

                    <div class="text-center mt-3">
                        <a href="javascript:void(0)" class="btn red btn-next">Próximo</a>
                    </div>
                </div><!-- END [ .STEP-1 ] -->
                <div class="item step-2 current" id="step-2">
                    <header>
                        <span>2</span> Dados Cadastrais
                    </header>
                    <main>
                        <h4>Dados de Acesso</h4>
                        <div class="form-group">
                            <input type="text"
                            name="email"
                            class="form-control"
                            placeholder="EMAIL"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preencha o campo com o seu email"
                            data-rule-email="true"
                            data-msg-email="Preencha o campo com um email válido"
                            >
                        </div>
                        <div class="text-center">
                            <a href="javascript:void(0)" class="btn pink btn-next" data-target="#step-3">Entrar</a>
                        </div>


                        <!-- FORMULÁRIO DE CADASTRO NA TELA DE CHECKOUT -->
                        <div class="py-5 d-none">
                            <div class="body pt-3">
                                <h5 class="text-pink mt-4">Dados Pessoais</h5>
                                <div class="form-group">
                                    <input type="text"
                                    name="nome"
                                    placeholder="NOME"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="cpf"
                                    placeholder="CPF"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    data-mask="cpf"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="escolaridade"
                                    placeholder="ESCOLARIDADE"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="data-nascimento"
                                    placeholder="DATA DE NASCIMENTO"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    data-mask="data"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="sexo"
                                    placeholder="SEXO"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <h5 class="text-pink mt-4">Contato</h5>
                                <div class="form-group">
                                    <input type="text"
                                    name="sexo"
                                    placeholder="TELEFONE"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    data-rule-minlength="14"
                                    data-msg-minlength="Preencha com um número válido"
                                    >
                                </div>
                                <h5 class="text-pink mt-4">Endereço</h5>
                                <div class="form-group">
                                    <input type="text"
                                    name="cep"
                                    placeholder="CEP"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    data-mask="cep"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="cidade"
                                    placeholder="CIDADE"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="endereco"
                                    placeholder="ENDEREÇO"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="estado"
                                    placeholder="ESTADO"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="bairro"
                                    placeholder="BAIRRO"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm">
                                        <div class="form-group">
                                            <input type="text"
                                            name="numero"
                                            placeholder="NÚMERO"
                                            class="form-control"
                                            required="required"
                                            data-rule-required="true"
                                            data-msg-required="Preenchimento Obrigatório"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm">
                                        <div class="form-group">
                                            <input type="text"
                                            name="complemento"
                                            placeholder="COMPLEMENTO"
                                            class="form-control"
                                            >
                                        </div>
                                    </div>
                                </div>

                                <h4 class="text-pink mt-3 mb-2">CRIE SUA SENHA</h4>
                                <div class="form-group">
                                    <input type="text"
                                    name="senha"
                                    placeholder="SENHA"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                    name="senha2"
                                    placeholder="REDIGITE A SENHA"
                                    class="form-control"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preenchimento Obrigatório"
                                    >
                                </div>

                                <div class="text-center">
                                    <a href="javascript:void(0)" class="btn pink" data-target="#step-3">Cadastrar</a>
                                </div>
                            </div>
                        </div>
                    </main>
                </div><!-- END [ .STEP-2 ] -->
                <div class="item step-3" id="step-3">
                    <header>
                        <span>3</span> Pagamento
                    </header>
                    <main>
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <button type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fas fa-angle-right"></i>
                                        Cartão de Crédito
                                    </button>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Nome (*) (impresso no cartão)</label>
                                            <input type="text" name="nome-cartao" class="form-control" placeholder="DIGITE O NOME IMPRESSO NO CARTÃO">
                                        </div>
                                        <div class="form-group">
                                            <label>Data de nascimento (*)</label>
                                            <input type="text" name="data-nascimento" class="form-control" placeholder="  /  /  " data-mask="data-nascimento">
                                        </div>
                                        <div class="form-group">
                                            <label>CPF titular cartão (*)</label>
                                            <input type="text" name="cpf-titular" class="form-control" placeholder="DIGITE O CPF DO TITULAR DO CARTÃO">
                                        </div>
                                        <div class="form-group">
                                            <label>Número do Cartão (*)</label>
                                            <input type="text" name="num-cartao" class="form-control" placeholder="DIGITE O NÚMERO IMPRESSO NO CARTAO">
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>VALIDADE (*)</label>
                                                    <select class="form-control" name="codigo-de-seguranca">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label></label>
                                                    <select class="form-control" name="codigo-de-seguranca">
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>Código de Segurança (*)</label>
                                                    <input type="text" name="cod-seguranca" class="form-control" placeholder="DIGITE O CÓDIGO DE SEGURANÇA">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Parcelas</label>
                                            <select class="form-control" name="qtde-parcelas">
                                                <option value="">ESPERANDO DADOS</option>
                                                <option value="1">1 Vez</option>
                                                <option value="2">2 Vez</option>
                                                <option value="3">3 Vez</option>
                                                <option value="4">4 Vez</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fas fa-angle-right"></i>
                                        Boleto
                                    </button>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Parcelas</label>
                                            <select class="form-control" name="codigo-de-seguranca">
                                                <option value="">ESPERANDO DADOS</option>
                                                <option value="1">1 Vez</option>
                                                <option value="2">2 Vez</option>
                                                <option value="3">3 Vez</option>
                                                <option value="4">4 Vez</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center mt-4">
                            <button type="submit" class="btn pink">
                                <strong>CONFIRMAR PAGAMENTO</strong>
                            </button>
                        </div>
                    </main>
                </div><!-- END [ .STEP-3 ] -->
            </div>
        </div>
    </form>

    <div class="container mt-3 d-flex">
        <p class="text-muted">Andresan - Cursos e Concursos - Todos os Direitos Reservados - 2018</p>
        <img src="<?php echo BASE_URL . 'assets/images/png/cards.png' ?>" alt="">
    </div>
</section>


<?php
elements('site/scripts');
elements('site/footer');
?>
