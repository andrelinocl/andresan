<?php
include 'config.php';
define('TITLE', '');
define('PAGE', 'video-aula-flat');

elements('site/header');

?>

<section data-component="videoaula-flat">
    <aside>
        <div class="aside-title">
            <span>TRF 4 Região</span>
            Técnico Juiciário - Área Administrativa

            <a href="javascript:void(0)" data-toggler="aside|active">
                <img src="<?php echo BASE_URL . 'assets/images/icons/close-button.svg' ?>" class="fd-svg">
            </a>

            <div class="aside-description">
                Qual disciplina que assistir?

                <div class="mocha-select">
                    <div class="mocha-select-header">
                        <span>Português</span>
                        <img src="" alt="">
                    </div>
                    <input type="text" name="" value="" class="mocha-select-input">
                    <div class="mocha-select-list">
                        <ul>
                            <li>Português</li>
                            <li>Matemática</li>
                        </ul>
                    </div>
                </div>

                <input type="text" name="search" placeholder="Busca" class="form-control mocha-search" data-target=".aside-items a span">
            </div>
        </div>



        <div class="aside-items">

            <div class="aside-items-title">Funções Sintáticas</div>
            <a href="#" data-status="assistido" data-video-player="youtube" data-video-id="roywYSEPSvc">
                <img src="<?php echo BASE_URL . 'assets/images/icons/tick.svg' ?>" class="fd-svg">
                <span>1: Sujeito; Objeto Direto - Aula 12A</span>
            </a>

            <a href="#" data-status="assistindo" data-video-player="youtube" data-video-id="roywYSEPSvc">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>2: Sujeito; Objeto Direto - Aula 12B</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="youtube" data-video-id="OjIxscGV-Qg">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>3: Sujeito; Objeto Indieto - Agente da passiva - Aula 12C</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>4: Sujeito; Objeto Direto - Aula 12D</span>
            </a>

            <div class="aside-items-title">Funções Sintáticas</div>
            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>1: Sujeito; Objeto Direto - Aula 12A</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>2: Sujeito; Objeto Direto - Aula 12B</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>3: Sujeito; Objeto Indieto - Agente da passiva - Aula 12C</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>4: Sujeito; Objeto Direto - Aula 12D</span>
            </a>

            <div class="aside-items-title">Funções Sintáticas</div>
            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>1: Sujeito; Objeto Direto - Aula 12A</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>2: Sujeito; Objeto Direto - Aula 12B</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>3: Sujeito; Objeto Indieto - Agente da passiva - Aula 12C</span>
            </a>

            <a href="#" data-status="pendente" data-video-player="vimeo" data-video-id="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/play.svg' ?>" class="fd-svg">
                <span>4: Sujeito; Objeto Direto - Aula 12D</span>
            </a>
        </div>
    </aside>

    <main>
        <div class="main-header">
            <a href="javascript:void(0)" class="small text-left order-1" data-toggler="aside|active">
                <span class="d-none d-xl-block">versão extendida</span>
                <i class="fas fa-bars d-xl-none"></i>
            </a>
            <a href="<?php echo BASE_URL . 'area-do-aluno.php' ?>" class="order-2 order-md-3">Ver outros cursos</a>
            <div class="main-title order-3 order-md-2">
                Aposto; Vocativo; Predicativo - Aula 12-D
            </div>
        </div>
        <div class="main-video">
            <?php elements('area-do-aluno/overlay-info'); ?>
            <?php elements('area-do-aluno/overlay-next'); ?>
            <?php elements('area-do-aluno/video-mask'); ?>

            <div class="embed-responsive embed-responsive-16by9">
                <!-- VIMEO -->
                <iframe id="embed-responsive__iframe" class="embed-responsive-item" data-type="vimeo" src="https://player.vimeo.com/video/313878877" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                <!-- YOUTUBE -->
                <!-- <iframe id="embed-responsive__iframe" type="text/html" src="https://www.youtube.com/embed/Z7CTnA3dTU0?rel=1&controls=1&showinfo=0&enablejsapi=1&origin=http://andresan-fe.oo" frameborder="0" allowfullscreen data-type="youtube"></iframe> -->
                <!-- <div id="embed-responsive__iframe" data-type="youtube" data-video-id="Z7CTnA3dTU0"></div> -->
            </div>
        </div>
    </main>
    <nav>
        <a href="#" data-title="Info" tooltip="Info" tooltip-position="left" class="btn-overlay">
            <img src="<?php echo BASE_URL . 'assets/images/icons/info.svg' ?>" alt="Info" class="fd-svg">
        </a>
        <a href="#" data-title="Buscar" tooltip="Buscar" tooltip-position="left">
            <img src="<?php echo BASE_URL . 'assets/images/icons/magnifying-glass.svg' ?>" alt="Buscar" class="fd-svg">
        </a>
        <a href="#" data-title="Caledário" data-target="calendar" tooltip="Calendário" tooltip-position="left">
            <img src="<?php echo BASE_URL . 'assets/images/icons/small-calendar.svg' ?>" alt="Calendário" class="fd-svg">
        </a>
        <a href="#" data-title="Reportar Erro" data-target="error-report" tooltip="Reportar Erro" tooltip-position="left">
            <img src="<?php echo BASE_URL . 'assets/images/icons/warning.svg' ?>" alt="Reportar Erro" class="fd-svg">
        </a>
        <a href="javascript:void(0)" tooltip="Favoritar" tooltip-position="left" class="favoritar">
            <div class="">
                <img src="<?php echo BASE_URL . 'assets/images/icons/fav.svg' ?>" alt="Favoritar" class="fd-svg">
            </div>
            <div class="d-none">
                <img src="<?php echo BASE_URL . 'assets/images/icons/fav-stroke.svg' ?>" alt="Favoritar" class="fd-svg pink">
            </div>
        </a>
        <a href="javascript:void(0)" class="change-light" tooltip="Apagar a Luz" tooltip-position="left">
            <img src="<?php echo BASE_URL . 'assets/images/icons/light-bulb.svg' ?>" alt="Apagar a Luz" class="fd-svg">
        </a>
        <a href="#" data-title="Slides" data-target="slides" class="bg" tooltip="Slides" tooltip-position="left">
            <img src="<?php echo BASE_URL . 'assets/images/icons/ppt.svg' ?>" alt="Slides" class="fd-svg">
        </a>
        <a href="#" data-title="Anotações" data-target="anotations" class="bg" tooltip="Anotações" tooltip-position="left">
            <img src="<?php echo BASE_URL . 'assets/images/icons/file.svg' ?>" alt="Anotações" class="fd-svg">
        </a>
        <a href="#" data-title="Materiais" data-target="materials" class="bg" tooltip="Materiais" tooltip-position="left">
            <img src="<?php echo BASE_URL . 'assets/images/icons/molesquine.svg' ?>" alt="Materiais" class="fd-svg">
        </a>
    </nav>
</section>

<div class="modal-flat">

    <a href="javascript:void(0)" data-toggler=".modal-flat|active">
        <img src="<?php echo BASE_URL . 'assets/images/icons/close-button.svg' ?>" class="fd-svg">
    </a>

    <div class="modal-flat-title"></div>
    <div class="modal-flat-content">
        <div data-element="calendar" class="d-none">
            <?php elements('area-do-aluno/flat/calendar'); ?>
        </div>
        <div data-element="error-report" class="d-none">
            Erro
        </div>
        <div data-element="slides" class="d-none">
            Slides
        </div>
        <div data-element="anotations" class="d-none">
            Anotações
        </div>
        <div data-element="materials" class="d-none">
            Material
        </div>
    </div>
</div>

<?php
elements('site/scripts');
elements('site/footer');
?>
