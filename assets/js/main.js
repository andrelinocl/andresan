var GLOBAL = [];

GLOBAL.MASK_BEHAVIOR = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
GLOBAL.MASK_OPTIONS = {
    onKeyPress: function(val, e, field, options) {
        field.mask(GLOBAL.MASK_BEHAVIOR.apply({}, arguments), options);
    }
};


$( document ).ready( function() {
    var OSName="Unknown OS";
    var body = document.querySelector('body');

    if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
    if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
    if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
    if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";

    if ( OSName == 'MacOS' ) {
        $('body').addClass('montserrat');
        // body.style.fontFamily = "'Montserrat', sans-serif !important";
        console.log('asd', body);
    }
})


/**
* Seta os svg
* @param s (boolean)
* @param c (class)
*/
function loadSVG(s, c) {
    if (s) {
        $(c).each(function () {
            var $img = $(this),
            imgID = $img.attr('id'),
            imgClass = $img.attr('class'),
            imgURL = $img.attr('src');

            $.get(imgURL, function (data) {
                var $svg = $(data).find('svg');
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }
                $svg = $svg.removeAttr('xmlns:a');
                if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                }
                $img.replaceWith($svg);
            }, 'xml');
        });
    }
}

// SET GLOBAL VARS
GLOBAL.BODY = $('body');
GLOBAL.HAMB = $('.hamb');
GLOBAL.VALIDATE = $('[data-validate]');
GLOBAL.MENU = $('.menu');
GLOBAL.TOGGLER = $('[data-toggler]');
GLOBAL.SENDING_TEMPLATE = $('<div class="loading"></div>');
GLOBAL.SENDED_TEMPLATE = $('<div class="message"></div>');
GLOBAL.FD_SELECT = $('.fd-select');
GLOBAL.FD_SELECT_2 = $('.fd-select-2');
GLOBAL.MOCHA_SELECT = $('.mocha-select');
GLOBAL.OWL = [];
GLOBAL.OWL.CURSOS_HOME = $('[data-carousel="spotlight-home"]');
GLOBAL.OWL.OUTROS_CURSOS = $('[data-carousel="outros-cursos"]');
GLOBAL.OWL.SPOTLIGHT_MODAL = $('[data-carousel="spotlight-modal"]');
GLOBAL.VIEW_MODE = $('.view-mode');

$(document).click(function (e) {
    e.stopPropagation();
    if (GLOBAL.FD_SELECT.has(e.target).length === 0) {
        GLOBAL.FD_SELECT.removeClass('active');
    }
    if (GLOBAL.FD_SELECT_2.has(e.target).length === 0) {
        GLOBAL.FD_SELECT_2.removeClass('active');
    }
    if (GLOBAL.MOCHA_SELECT.has(e.target).length === 0) {
        GLOBAL.MOCHA_SELECT.removeClass('active');
    }
    if ($('.dropdown-login').has(e.target).length === 0 ) {
        $('.dropdown-login-content').removeClass('active');
    }
});

$(document).ready(function() {

    loadSVG(true, '.fd-svg');

    $('[data-mask="cpf"]').mask('000.000.000-00', {reverse: true});
    $('[data-mask="phone"]').mask(GLOBAL.MASK_BEHAVIOR, GLOBAL.MASK_OPTIONS);
    $('[data-mask="cep"]').mask('00000-000');
    $('[data-mask="nascimento"]').mask('00/00/0000');

    // TOGGLE MENU
    GLOBAL.HAMB.on('click', function() {
        GLOBAL.MENU.addClass('active');
        GLOBAL.BODY.addClass('navbar-open');
    });

    $('.close-menu').on('click', function() {
        GLOBAL.MENU.removeClass('active');
        GLOBAL.BODY.removeClass('navbar-open');
    });

    // TEMPLATE DE ENVIO DO FORMULÁRIO
    if( GLOBAL.SENDING_TEMPLATE.length ){
        GLOBAL.SENDING_TEMPLATE.append('<div>' +
        '<div class="text">ENVIANDO</div>' +
        '</div>'
    )}

    if( GLOBAL.SENDED_TEMPLATE.length ){
        GLOBAL.SENDED_TEMPLATE.append('<div>' +
        '<div class="text"></div>' +
        '</div>'
    )}

    $('.validate-login').validate({

    });

    GLOBAL.VALIDATE.each( function() {
        $(this).validate({
            submitHandler: function( form ){
                var DATA = $( form ).serialize();
                var BUTTON = $( form ).find('button[type="submit"]');

                BUTTON.attr('disabled', true);

                $( form ).append(GLOBAL.SENDING_TEMPLATE);

                setTimeout( function() {
                    $( form )
                    .find('.loading')
                    .remove();


                    if ( $( form ).data('modal') != undefined) {
                        $( $( form ).data('modal') ).addClass('active');
                    } else {
                        $( form ).append(GLOBAL.SENDED_TEMPLATE);
                        GLOBAL.SENDED_TEMPLATE
                        .find('.text')
                        .html('Sua mensagem foi enviada com sucesso');
                    }

                    setTimeout( function() {
                        if ( GLOBAL.SENDED_TEMPLATE.length ) {
                            GLOBAL.SENDED_TEMPLATE.remove();
                        }

                        BUTTON.attr('disabled', false);
                    }, 5000 );

                }, 8000);
            }
        });
    });

    // TOGGLE EVENT
    GLOBAL.TOGGLER.on('click', function(e) {
        e.preventDefault();
        var data = $(this).data('toggler'),
        trgt = data.split('|')[0],
        clss = data.split('|')[1];
        $(trgt).toggleClass(clss);
    });


    // SLICK SLIDER
    if ( GLOBAL.OWL.CURSOS_HOME.length ) {
        GLOBAL.OWL.CURSOS_HOME.owlCarousel({
            loop:false,
            margin:10,
            nav:false,
            responsive:{
                0:{
                    items: 1
                },
                576 : {
                    items: 2
                },
                992 : {
                    items: 3
                }
            }
        });
    }

    if ( GLOBAL.OWL.OUTROS_CURSOS.length ){
        GLOBAL.OWL.OUTROS_CURSOS.owlCarousel({
            loop : false,
            margin : 10,
            nav : false,
            responsive : {
                0 : {
                    items: 1
                },
                576 : {
                    items: 2
                },
                768 : {
                    items: 3
                },
                992 : {
                    items: 4
                }
            }
        });
    }

    if ( GLOBAL.OWL.SPOTLIGHT_MODAL.length ){
        GLOBAL.OWL.SPOTLIGHT_MODAL.owlCarousel({
            loop : true,
            margin : 10,
            nav : false,
            responsive : {
                0 : {
                    items: 1
                }
            }
        });

        $('.spotlight_modal--next').click(function() {
            GLOBAL.OWL.SPOTLIGHT_MODAL.trigger('next.owl.carousel');
        })
        // Go to the previous item
        $('.spotlight_modal--prev').click(function() {
            // With optional speed parameter
            // Parameters has to be in square bracket '[]'
            $(GLOBAL.OWL.SPOTLIGHT_MODAL).trigger('prev.owl.carousel');
        })
    }

    // CHANGE VIEW MODE FROM COURSE LIST
    GLOBAL.VIEW_MODE.find('a').on( 'click' , function(ev) {
        ev.preventDefault();

        var _this = $(this);

        if ( $(_this).data('target') != undefined ) {
            $(_this).siblings('a').removeClass('selected');
            $(_this).addClass('selected');


            var a = $(_this).data('target');
            var b = a.split(" ");
            console.log(b);

            if ( b[0].charAt(0) == '@') {
                var res = '[data-component="' + b[0].substring(1) + '"] ' + b[1];
                console.log(res);
                $('[data-component="' + b[0].substring(1) + '"] ' + b[1]).attr('data-grid', $(_this).data('style'));
            } else {
                $(a).attr('data-grid', $(_this).data('style'));
            }
        }
    })

    //FD SELECT
    GLOBAL.FD_SELECT.find('.fd-select-title').on('click', function() {
        var _this = $(this);
        var _parent = $(this).closest('.fd-select');

        if ( !$(_parent).hasClass('active') ) {
            $('.fd-select').removeClass('active');
            $(_parent).addClass('active');
        } else {
            $(_parent).removeClass('active');
        }

        GLOBAL.FD_SELECT.find('ul li a').on('click', function(event) {
            event.preventDefault();

            $(this)
            .closest(GLOBAL.FD_SELECT)
            .find('span')
            .text($(this).text());
            $(_parent).removeClass('active');
        })
    });

});


$( document ).ready( function() {
    var MOCHA = [];
    MOCHA.el = $('.mocha-select');
    MOCHA.header = $('.mocha-select-header');
    MOCHA.input = $('.mocha-select-input');
    MOCHA.list = $('.mocha-select-list');

    if ( MOCHA.el.length ) {
        console.log('MOCHA - initialized');
        if( MOCHA.input.val() != ''){
            MOCHA.header.find('span').text(MOCHA.input.val());
        } else { MOCHA.header.find('span').text('Selecione'); }

        $(MOCHA.header.find('span')).on('click', function () {
            MOCHA.el.toggleClass('active');
        });

        $(MOCHA.list.find('li').on('click', function() {
            MOCHA.el.removeClass('active');
            MOCHA.header.find('span').text($(this).html());

            MOCHA.input.val($(this).text());
        }));

        console.log('MOCHA - is complete');
    }
});

$( document ).ready( function() {
    MOCHA = [];
    MOCHA.el = $('.mocha-search');
    MOCHA.tar = MOCHA.el.data('target');
    MOCHA.el.on('keyup', function() {

    });
});

$( document ).ready( function() {
    var FDS2 = [];
    FDS2.el = $( '.fd-select-2' );
    FDS2.el.find( '.fds-header' ).on('click', function( ev ) {
        ev.preventDefault();
        var self = $(this),
        parent = $(self).closest(FDS2.el);
        $(parent).find( '.fds-body li' ).on('click', function() {
            $(parent).find( '.fds-header span' ).html($(this).text());
            $(parent).removeClass('active');
            var a = $(parent).data('input');
            $(this).siblings().removeClass('selected');
            $(this).addClass('selected');
            $(parent).find('input[name="' + a + '"]').val($(this).data('value'));
        })
    });
});

$( document ).ready( function() {
    var FDS3 = [];
    FDS3.el = $( '.fd-select-3' );

    FDS3.el.find( '.fd-select-header' ).on('click', function( ev ) {
        ev.preventDefault();
        var self = $(this),
        parent = $(self).closest(FDS3.el);

        $(parent).toggleClass('active');
        $(parent).find( '.fd-select-body a' ).on('click', function() {
            $(parent).find( '.fd-select-header span' ).html($(this).text());
            $(parent).removeClass('active');
            var a = $(parent).data('input');
            $(parent).find('input[name="' + a + '"]').val($(this).data('value'));
        });

        $(parent).find('input').keyup(function( event ) {

            var filter = $(this).val();
            $(parent).find( '.fd-select-body a' ).each(function(){
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).addClass('d-none');
                } else {
                    $(this).removeClass('d-none');
                }
            })
        });
    });
});


$( document ).ready( function() {
    var FORM = [];
    FORM.el = $('[data-component="cadastro"] form');
    FORM.fields = [];


    $(".form-cadastro .cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $(".form-cadastro .estado").val("...");
                $(".form-cadastro .cidade").val("...");
                $(".form-cadastro .endereco").val("...");
                $(".form-cadastro .bairro").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    console.log(dados);

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $(".form-cadastro .estado").val(dados.uf);
                        $(".form-cadastro .cidade").val(dados.localidade);
                        $(".form-cadastro .endereco").val(dados.logradouro);
                        $(".form-cadastro .bairro").val(dados.bairro);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
})


$( document ).ready( function() {
    var EL = $('[data-component="institucional"] .teacher');
    EL.on('click', function() {
        var figure = $(this).find('figure').css('background-image'),
        text = $(this).find('.caption p').html(),
        name = $(this).find('.caption .name').data('name'),
        modal = $('.modal-teacher');

        $(modal).find('h4 span').html(name);
        $(modal).find('.content p').html(text);
        $(modal).find('figure').css({
            'background-image' : figure
        })
        console.log(figure, name, text);
    });
});


$( document ).ready( function() {
    $('.change-light').on('click', function() {

        if ( !$(this).hasClass('selected') ) {
            $(this).addClass('selected');
            $(this).css({
                'position' : 'relative',
                'z-index' : '9990'
            });
            $('.embed-responsive').css({
                'position' : 'relative',
                'z-index' : '9990'
            });
            $('.sidebar-video').css({
                'position' : 'relative',
                'z-index' : '9990'
            })
        } else {
            $(this).removeClass('selected');
            $('.change-light').removeAttr('style');
            $('.embed-responsive').removeAttr('style');
        }

        if ( $('.body-bg').length == 0){
            $('body').append('<div class="body-bg"></div>');
        }
        if ( $('.body-bg').length == 1){
            $('.body-bg').on('click', function() {
                $(this).removeClass('active');
                $('.change-light').removeAttr('style').removeClass('selected');
                $('.embed-responsive').removeAttr('style');
            });
        }

        setTimeout( function() {
            $('.body-bg').toggleClass('active');
        }, 100);
    });

    $('.change-size a').on('click', function() {
        var el = $('.video-section'),
        sidebar = $(el).find('.sidebar-video'),
        video = $(el).find('.row-video__content');

        $(this).siblings('a').removeClass('selected');
        $(this).addClass('selected');

        $(video).attr('data-width' , $(this).data('width').split('|')[0]);
        $(sidebar).attr('data-width' , $(this).data('width').split('|')[1]);
    });
});

$( document ).ready( function() {
    var CHECKOUT = [];

    CHECKOUT.carousel = $('[data-carousel="checkout"]');

    CHECKOUT.carousel.owlCarousel({
        items: 1,
        loop: false,
        responsive : {
            576 : {
                items: 2
            },
            992 : {
                items: 3
            }
        }
    })
});

$('.modal-pesquisa .radio-text input[type=radio]').on('click', SetPlaceholder(), ChangePlaceholder);

function SetPlaceholder(){
    var a = $('.modal-pesquisa input[type=radio]:checked'),
    b = $(a).siblings().attr('data-placeholder'),
    c = $('.modal-pesquisa .form-control');

    $(c).attr('placeholder', b);
}

function ChangePlaceholder(){
    var t = $(this),
    placeholder = $(t).siblings().data('placeholder'),
    input = $(t).parents('form').find('input.form-control');

    input.attr('placeholder', placeholder);
}

$( document ).ready( function() {
    $('[data-component="videoaula-flat"] nav a[data-target]').on('click', function(ev) {
        ev.preventDefault();
        var modal = $('.modal-flat');

        $(modal).addClass('active');
        $(modal).find('.modal-flat-title').html($(this).data('title'));
        $(modal).find('[data-element]').addClass('d-none');
        $(modal).find('[data-element="'+ $(this).data('target') +'"]').toggleClass('d-none');

    });

    // FAVORITA A AULA
    $('[data-component="videoaula-flat"] nav a.favoritar').on('click', function() {
        $(this).toggleClass('selected');
        $(this).find('div').toggleClass('d-none');
    });

    // EXIBE O OVERLAY NO VIDEO
    $('[data-component="videoaula-flat"] .btn-overlay').on('click', function() {
        var main = $('[data-component="videoaula-flat"] main');
        $(main).find('.main-overlay').toggleClass('active');
        $(main).find('.main-overlay-info').toggleClass('active');
    });
});

$( window ).load( function() {
    // $('[data-component="videoaula-flat"] .main-overlay-next').addClass('active');
})


// CHECKOUT
$( document ).ready( function() {
    // SET VALIDATE TO FORM
    $('[data-component="checkout"] form').each(function () {
        $(this).validate();
    });

    $('.curso-remove').on('click', function() {
        $(this).closest($(this).data('target')).remove();
    });

    $('[data-component="checkout"] form').find('a.btn-next').on('click', function () {
        var inputs = $(this).closest('.item').find('input');

        inputs.each( function () {

            if ( $(inputs).valid() ) {
                $(this).closest('.form-group').find('label.error').remove();
            }
        });

        if ( $(this).closest('.item').find('label.error').length === 0 ){
            $(this).closest('.current').removeClass('current');
            $('[data-carousel="checkout"]').trigger('next.owl.carousel');
            $($(this).data('target')).addClass('current');
        }
    });
});

// FUNCOES DO VIMEO
$( document ).ready( function() {
    var iframe = document.querySelector('#embed-responsive__iframe');
    var playButton = $('#video-mask__button');
    var el = $(iframe).data('type');
    var videoId = $(iframe).data('video-id');
    var next = $('.main-overlay-next__image');

    if ( el == 'vimeo' ) {
        var playerVimeo = new Vimeo.Player(iframe);

        playButton.on('click', function() {
            playerVimeo.play();
            $(playButton).removeClass('active');
        });

        playerVimeo.on( 'pause', function() {
            if ( !$(iframe).hasClass('ended') ) {
                $(playButton).addClass('active');
            }
        });

        playerVimeo.on( 'ended', function() {
            $('.main-overlay-next').addClass('active');
            $(iframe).addClass('ended');
            $(playButton).removeClass('active');
        });

        $('.main-overlay-next__again, .playAgain').on('click', function() {
            playerVimeo.play();
            $(playButton).removeClass('active');
            $('.main-overlay-next').removeClass('active');
            $('.main-overlay-info').removeClass('active');
            $(iframe).removeClass('ended');
        });



    } else if ( el == 'youtube' ) {

        // 2. This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var player;
        window.onYouTubePlayerAPIReady = function () {
            player = new YT.Player('embed-responsive__iframe', {
                events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange}
            });
        };

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
            // event.target.playVideo();
        }
        function onPlayerStateChange(event) {

            switch (event.data) {
                case YT.PlayerState.ENDED:
                playButton.removeClass('active');
                $('[data-component="videoaula-flat"] .main-overlay-next').addClass('active');
                break;
                case YT.PlayerState.PLAYING:
                playButton.removeClass('active');
                break;
                // case YT.PlayerState.PAUSED:
                //     playButton.addClass('active');
                // break;
                // case YT.PlayerState.BUFFERING:
                //     playButton.removeClass('active');
                // break;
            }
        }

        next.on('click', function() {
            player.nextVideo();
        });

        playButton.on('click', function() {
            player.playVideo();
        });

        $('.main-overlay-next__again, .playAgain').on('click', function() {
            player.playVideo();
            $(playButton).removeClass('active');
            $('.main-overlay-next').removeClass('active');
            $('.main-overlay-info').removeClass('active');
        });
    }
});


$( document ).ready( function() {
    var locale = $('.fd-locale');

    locale.find('[data-next]').on('click', function() {
        var step = $(this).data('next');

        console.log(step.split('-')[0], step.split('-')[1]);

        $(this).closest('[data-step]').removeClass('active');
        $('[data-' + step.split('-')[0] + '=' + step.split('-')[1] + ']').addClass('active');
    });
});



$( document ).ready( function() {
    var mocha = [];
    var err = false;

    mocha.el = $('.mocha__select');
    mocha.uf = estados;
    mocha.ul = $(mocha.el).find('ul');

    var location = '';

    if ( mocha.el.length ){

        // $('.fd-locale__backdrop').on('click', function() {
        //     $(mocha.el).fadeOut(300);
        //     $('.fd-locale').fadeOut(300);
        // });

        $('.fd-locale__form .form-group').append('<p class="fd-locale__info"></p>');

        $.each( mocha.uf, function( i, item ) {
            var element = '<li class="mocha__select-list-item" data-arr="' + mocha.uf[i].sigla.toLowerCase() + '|' + mocha.uf[i].slug + '|' + mocha.uf[i].nome.toLowerCase() + '">' + mocha.uf[i].nome + '</li>';
            $(mocha.el).find('ul').append( element );
        })

        $(mocha.el).find('input').on('keyup', function() {
            var texto = $(this).val().toLowerCase();
            var li = $(mocha.el).find('ul li');

            $(li).each(function(){
                if ( $(this).filter('[data-arr *= ' + texto + ']').length > 0 || texto.length < 1 ){
                    $(this).show();
                    // location =  $(this).data('arr').split('|')[1] + ' - ' + $(this).data('arr').split('|')[0];
                } else {
                    $(this).hide();
                }
            });

            if ( $(this).val() ) {
                $(mocha.ul).css({ 'display' : 'block' });
            } else {
                $(mocha.ul).css({ 'display' : 'none' });
            }
        });

        $(mocha.ul).find('li').on('click', function() {
            var t = $(this).text()
            $('body').attr('data-state', t);
            $(mocha.ul).css({ 'display' : 'none' });

            console.log($(this).text());

            setTimeout( function() {
                $('[data-component="top-menu"] .social p strong').text( t );
                $('.fd-locale').removeClass('active');
                $(mocha.el).find('input').val('');
            }, 500);
        });
    }

    window.addEventListener('keydown', function(e) {
        var l = e.keyCode;

        // if ( l == 13 ){
        //     for (var i = 0; i < mocha.uf.length; i++) {
        //         var inp = $(mocha.el).find('input').val();
        //         if ( inp == mocha.uf[i].nome || inp == mocha.uf[i].slug ) {
        //             $(mocha.el).fadeOut(300);
        //             $('.fd-locale').fadeOut(300);
        //         } else {
        //             err = true;
        //         }
        //     }
        // }

        if ( l === 27 ) {
            $('.fd-locale').removeClass('active');
        }
    }, true);
});

$( document ).ready( function() {
    // vars

    'use strict';

    var Shuffle = window.shuffle;

    var Filter = function( element ) {
        this.category = Array.from( document.querySelectorAll('.filter .select__categoria a'));
        this.modality = Array.from( document.querySelectorAll('.filter .select__modalidade a'));
        this.course = Array.from( document.querySelectorAll('.filter .select__curso a'))

        this.shuffle = new Shuffle( element, {
            easing : 'cubic-bezier(0.165, 0.840, 0.440, 1.000)',
            sizer : '.sizer'
        });

        this.filters = {
            category : [],
            modality : [],
            course : []
        }

        this._bindEventListeners();

        Filter.prototpe._bindEventListeners = function() {
            this._onCategoryChange = this._handleCategoryChange.bind(this);
            this._onModalityChange = this._handleModalityChange.bind(this);
            this._onCourseChange = this._handleCourseChange.bind(this);

            this.category.forEach( function( href ) {
                href.addEventListener('change', this._onCategoryChange);
            });

            this.modality.forEach( function( href ) {
                href.addEventListener('change', this._onModalityChange);
            });

            this.course.forEach( function( href ) {
                href.addEventListener('change', this._onCourseChange);
            });
        }

        // crio o filtro pela categoria
        Filter.prototype._getCurrentCategoryFilters = function () {
            return this.category.filter( function( a ) {
                return a.classList.contains('active');
            }).map( function( a ) {
                return a.getAttribute('data-category');
            })
        };


        Filter.prototype._handleCategoryChange = function( ev ) {
            var btn = ev.currentTarget;

            this.filters.category = this._getCurrentCategoryFilters();
            this.filter();
        }

        Filter.prototype.itemPassesFilters = function(el) {
            var categories = this.filters.category;
            var category = el.getAttribute('data-category');

            if ( categories.length >0 && !categories.includes(category) ) {
                return false;
            }

            return true;
        };

        document.addEventListener('DOMContentLoaded', function() {
            window.filter = new Filter(document.querySelectorAll('.filter-courses'));
        })
    }
});
