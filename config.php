<?php

// DEFINE A BASE URL
if ( isset( $_SERVER["HTTPS"] ) && strtolower( $_SERVER["HTTPS"] ) == "on" ) {
    $protocol = ($_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';;
    $host = $_SERVER['HTTP_HOST'];
} else {
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
    $host = $_SERVER['HTTP_HOST'];
}

$url = $protocol . $host .'/';

// echo '<pre>';
// print_r($_SERVER);die();
define('BASE_URL', $url);



// FUNCTIONS
// CRIO A FUNCAO DE CARREGAMENTO DOS COMPONENTES
if( !function_exists('elements') ) {
    function elements( $local ) {
        include( 'elements/' . $local . '.php' );
    }
}


// CRIO A FUNCAO DE CARREGAMENTO DOS CSS
if( !function_exists('css') ) {
    function css( $css = null, $rel = 'stylesheet', $type = 'css' ){
        return '<link rel="' . $rel . '" type="text/' . $type . '" href="' . $css . '.css">' . "\n";
    }
}

// CRIO A FUNCAO DE CARREGAMENTO DOS SCRIPTS
if( !function_exists('js') ){
    function js( $js = null, $type = 'javascript' ) {
        return '<script type="text/' . $type . '" src="'. $js .'.js"></script>' . "\n";
    }
}
